﻿import { Component } from '@angular/core';
import { OrderApiService } from './Services/orderApi.service';
import { OrderDetailsComponent } from './Orders/orderDetails.component';
import { CustomerDetailsComponent } from './Customers/customerDetails.component';
import { RouterModule, Routes } from '@angular/router';
import { CustomersComponent } from './Customers/customers.component';
import { OrdersComponent } from './Orders/orders.component';
import { QuotesComponent } from './Quotes/quotes.component';
import { AuthGuardService } from './Services/OAuth/authguard.service';
import { LoggedOutComponent } from './Services/OAuth/loggedout.component';
import { SigninComponent } from './Signin/signin.component';
import { QuoteDetailsComponent } from './Quotes/quoteDetails.component';
import { ContactsComponent } from './Contacts/contacts.component';
import { ContactDetailsComponent } from './Contacts/contactDetails.component';
import { DirtyFormGuardService } from './Services/dirtyFormGuard.service';
// import { DirtyFormExistsGuardService } from './Services/dirtyFormExistsGuard.service';

export const routes: Routes = [
     {   path: 'orders', 
        component: OrdersComponent , 
        canActivate: [AuthGuardService],
        // canDeactivate: [ DirtyFormGuardService],
        children: [
            // {
            //     path: 'customer-details/:customerNumber',
            //     component: CustomerDetailsComponent
            // },
            {
                path: 'order-details/:orderNumber',
                component: OrderDetailsComponent,
                canDeactivate: [ DirtyFormGuardService],
                // canActivate: [DirtyFormExistsGuardService],
            }
            // {
            //     path: 'quote-details/:quoteNumber',
            //     component: CustomerDetailsComponent
            // }

        ]
    },  
    {   path: 'customers', 
        component: CustomersComponent, 
        canActivate: [AuthGuardService], //DirtyFormExistsGuardService],
        children: [
            // {
            //     path: 'order-details/:orderNumber',
            //     component: OrderDetailsComponent
            // },
            {
                path: 'customer-details/:customerNumber',
                component: CustomerDetailsComponent,
                canDeactivate: [ DirtyFormGuardService],
                // canActivate: [DirtyFormExistsGuardService],
            }
            // {
            //     path: 'quote-details/:quoteNumber',
            //     component: CustomerDetailsComponent
            // }
        ]
    }, 
    // {   path: 'customer-details/:customerNumber', component: CustomerDetailsComponent, canActivate: [AuthGuardService]}, 

    {   path: 'quotes',
        component: QuotesComponent,
        canActivate: [AuthGuardService], // DirtyFormExistsGuardService],
        children: [
            {   path: 'quote-details/:quoteNumber',
                component: QuoteDetailsComponent,
                canDeactivate: [ DirtyFormGuardService],
                // canActivate: [DirtyFormExistsGuardService],
            }
            // {   path: 'customer-details/:customerNumber',
            //     component: CustomerDetailsComponent
            // },
            // {
            //     path: 'order-details/:orderNumber',
            //     component: OrderDetailsComponent
            // }
        ]
    },
    // { path: 'quote-details/:quoteNumber', component: QuoteDetailsComponent, canActivate: [AuthGuardService] },
    
    {   path: 'contacts',
        component: ContactsComponent,
        canActivate: [AuthGuardService],
        children: [
            // {   path: 'quote-details/:quoteNumber',
            //     component: QuoteDetailsComponent
            // },
            // {   path: 'customer-details/:customerNumber',
            //     component: CustomerDetailsComponent
            // },
            // {
            //     path: 'order-details/:orderNumber',
            //     component: OrderDetailsComponent
            // },
            {
                path: 'contact-details/:contactId',
                component: ContactDetailsComponent,
                canDeactivate: [ DirtyFormGuardService],
            }
        ]
    },
    // { path: 'contact-details/:contactId', component: ContactDetailsComponent, canActivate: [AuthGuardService, DirtyFormExistsGuardService] },
    { path: 'signin', component: SigninComponent },
    { path: 'logout', component: LoggedOutComponent },    
    // { path: '**', component: OrdersComponent, canActivate: [AuthGuardService]} //canActivate: [AuthGuardService] },
    { path: '**', redirectTo: '/orders', canActivate: [AuthGuardService]} //canActivate: [AuthGuardService] },

];

export const routing = RouterModule.forRoot(routes/*, { enableTracing: true }*/);