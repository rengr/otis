import { SearchService } from '../Services/search.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { NotificationService } from '../Services/notification.service';
import { Observable ,  BehaviorSubject ,  Subject } from 'rxjs';
import { Router } from '@angular/router';

import { CustomerApiService } from '../Services/customerApi.service';
import { Customer } from '../Models/customerModels';


@Component({
    selector: 'customerlist',
    templateUrl: './customers.component.html',
    styleUrls: ['./customers.component.scss'],
})
export class CustomersComponent implements OnDestroy, OnInit {
    headers$: Observable<string[]>;
    columns$: Observable<string[]>;
    customers$: Customer[]; 
    showDetailsBoolean: boolean = false;
    searchTerm$ = new Subject<string >();

    constructor(
        private customerService: CustomerApiService,
        private router: Router,
        private notification: NotificationService,
        private search: SearchService
    ) {
        this.headers$ = new BehaviorSubject([
            'Name',
            'Short Name',
            'Shipping Account',
            'Notes',
            'Template Notes',
            'Backend Name',
            'FTP Username',
        ]);
        this.columns$ = new BehaviorSubject([
            'name',
            'shortName',
            'shippingAccount',
            'notes',
            'templateNotes',
            'backendUserName',
            'ftpUserName',
        ]);
    }

    comparer = (a,b) => {
        //console.log('a ', a.shortName, 'b ', b.shortName);
            if(a.shortName.toLowerCase() < b.shortName.toLowerCase()) return -1;
            if(a.shortName.toLowerCase() > b.shortName.toLowerCase()) return 1;
            return 0;
    };


    ngOnInit(): void {
        this.customerService.customerList$.subscribe(
                data => { 
                    this.customers$ = data;
                    this.customers$.sort(this.comparer);
                },
                error => console.log('send this error to the error service', error),
                () => console.log('completed deleted'));
        this.customerService.fetchCustomerList();
        
        this.customerService.searchCustomers(this.searchTerm$);
        
        this.customerService.announcingChangedCustomer$.subscribe(customer => {
                var searchTerm = <HTMLInputElement>document.getElementById('searchBox');
                this.searchTerm$.next(searchTerm.value);
        });
    }

    ngOnDestroy() {

    }

    onActivate(event: any) {
        this.showDetailsBoolean = true;
    }
    onDeactivate(event: any) {
        this.showDetailsBoolean = false;
    }

}
