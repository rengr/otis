
import { map } from 'rxjs/operators';
import { NotificationService } from '../Services/notification.service';
import { Customer } from '../Models/customerModels';
import { Component, OnDestroy, OnInit, HostListener } from '@angular/core';
import { Observable } from 'rxjs';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CustomerApiService } from '../Services/customerApi.service';
import { PartnerApiService } from '../Services/partnerApi.service';
import { Partner } from '../Models/partnerModel';
import { UtilityService } from '../Services/utilities';
import { NotificationType } from '../Models/notificationModel';
import { DialogService } from '../Services/dialog.service';


@Component({
    templateUrl: './customerDetails.component.html',
    selector: 'customer-details'
})
export class CustomerDetailsComponent implements OnDestroy, OnInit {
    //customerNumber: number;
    organizationId: number = null;
    orgIsCreated: boolean = false;
    form: FormGroup;
    partners$: Observable<Partner[]>;
    showpartners: boolean = true;

    constructor(
        private customerApiService: CustomerApiService,
        private partnerApiService: PartnerApiService,
        private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder,
        private notification: NotificationService,
        private utils: UtilityService,
        private dialogService: DialogService

    ) {
        this.createForm();
        this.partners$ = this.partnerApiService.partnerList$.pipe(
            map(list => {
                return list.sort((a, b) => {
                    if (a.name.toLocaleLowerCase() > b.name.toLocaleLowerCase()) { return 1; };
                    if (a.name.toLocaleLowerCase() < b.name.toLocaleLowerCase()) { return -1; };
                    return 0;
                });
            }));


    }

    ngOnInit(): void {
        this.route.paramMap.pipe(
            map((params) => +(params.get('customerNumber'))))
            .subscribe((custNumber) => {
                if (custNumber == 0) {
                    this.updateForm(new Customer());
                } else {
                    this.customerApiService.getCustomer(custNumber)
                        .subscribe(result => { this.updateForm(result); });
                    this.customerApiService.getOrders(custNumber)
                        .subscribe(projects => { this.updateProjects(projects); });
                    this.customerApiService.getHandler(custNumber)
                        .subscribe(handler => { this.updateHandler(handler); })
                }
            });
    }

    private createForm() {
        this.form = this.formBuilder.group({
            name: '',
            shortName: '',
            partnerId: null,
            ftpUserName: '',
            ftpPassword: '',
            backendUserName: '',
            backendPassword: '',
            notes: '',
            templateNotes: '',
            shippingAccount: '',
            projectList: '',
            handler: ''
        });
    }
    private updateForm(customer: Customer) {
        let form = this.form;
        this.organizationId = customer.organizationId;
        this.orgIsCreated = customer.organizationId > 0;
        form.patchValue({
            name: customer.name,
            shortName: customer.shortName,
            partnerId: customer.partnerId,
            ftpUserName: customer.ftpUserName,
            ftpPassword: customer.ftpPassword,
            backendUserName: customer.backendUserName,
            backendPassword: customer.backendPassword,
            notes: customer.notes,
            templateNotes: customer.templateNotes,
            shippingAccount: customer.shippingAccount,
        });
        form.markAsPristine();
    }


    private updateProjects(projectList: any) {
        if (projectList == null || projectList.Name == null) { return; }
        this.form.patchValue({ projectList: projectList.Name });
    }

    private updateHandler(handler: any) {
        if (handler == null || handler.Name == null) { return; }
        this.form.patchValue({ handler: handler.Name });

    }


    onSubmit() {
        if (this.form.invalid) { return; }
        let customer = this.prepareSaveCustomer();
        if (customer.organizationId > 0) {
            this.customerApiService.updateCustomer(customer)
                .subscribe(
                    (updatedCustomer: Customer): void => {
                        this.updateForm(updatedCustomer);
                        this.notification.sendNotification('Customer has been updated. ', null);
                    },
                    error => {
                        debugger;
                        console.log(error);
                    });
        } else {
            this.customerApiService.createCustomer(customer)
                .subscribe(
                    (newCustomer: Customer): void => {
                        this.updateForm(newCustomer);
                        this.router.navigate(['../../customer-details', newCustomer.organizationId], { relativeTo: this.route });
                    },
                    error => {
                        debugger;
                        console.log(error);
                    });
        }
    }

    onHideOrg() {
        this.customerApiService.hideCustomer(this.organizationId)
            .subscribe(
                didHide => {
                    if (didHide) {
                        this.notification.sendNotification('Organization has been hidden', NotificationType.info);
                        this.router.navigate(['../..'], { relativeTo: this.route });
                    }
                }
            );
    }

    private prepareSaveCustomer(): Customer {
        const formModel = this.form.value;
        const util = this.utils;
        const saveCustomer: Customer = new Customer();
        saveCustomer.organizationId = this.organizationId;
        saveCustomer.name = util.trimmedStrOrNull(formModel.name);
        saveCustomer.shortName = util.trimmedStrOrNull(formModel.shortName);
        saveCustomer.partnerId = util.valueOrNull(formModel.partnerId);
        saveCustomer.ftpUserName = util.trimmedStrOrNull(formModel.ftpUserName);
        saveCustomer.ftpPassword = util.trimmedStrOrNull(formModel.ftpPassword);
        saveCustomer.backendUserName = util.trimmedStrOrNull(formModel.backendUserName);
        saveCustomer.backendPassword = util.trimmedStrOrNull(formModel.backendPassword);
        saveCustomer.notes = util.trimmedStrOrNull(formModel.notes);
        saveCustomer.templateNotes = util.trimmedStrOrNull(formModel.templateNotes);
        saveCustomer.shippingAccount = util.trimmedStrOrNull(formModel.shippingAccount);
        return saveCustomer;
    }
    showPartners() {
        this.showpartners = !this.showpartners;
    }


    canDeactivate(): Observable<boolean> | boolean {
        if (!this.form.pristine) {
            return this.dialogService.confirm('YOU WILL LOSE YOUR CHANGES IF YOU CONTINUE');
        }
        return true;
    }

    @HostListener('window:beforeunload', ['$event'])
    public beforeunloadHandler($event) {
        if (!this.form.pristine) {
            $event.returnValue = "Discard changes for this Order?";
        }
    }

    ngOnDestroy() {

    }
}