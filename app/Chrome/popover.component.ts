import { AfterViewInit, ElementRef, HostListener, Component, Output, EventEmitter, Input } from "@angular/core";
import { FilterState, filterListItem } from '../Models/filterStateModel';

@Component({
  selector: 'popover',
  templateUrl: './popover.component.html',   
  styleUrls: ['./popover.component.scss'],
  // styleUrls: ['../Orders/orders.component.scss'],
})
export class PopOverComponent implements AfterViewInit {
    private parentNode: any;
    @Output() closeEventEmmit: EventEmitter<any> = new EventEmitter();
    @Output() sortOrderEventEmmit: EventEmitter<any> = new EventEmitter();
    @Output() filtersEventEmmit: EventEmitter<any> = new EventEmitter();
    @Input() tempString: string;
    @Input() filterState: FilterState;
    myFilterList : filterListItem[];
    columnName : string;
    AtoZChecked : boolean = false;
    ZtoAChecked : boolean = false;

     constructor(
       private _element: ElementRef
      //  private _filterState: FilterState
     ) { }
   
     ngAfterViewInit(): void {
       this.parentNode = this._element.nativeElement.parentNode;
     }
   
     @HostListener('document:click', ['$event.path'])
     onClickOutside($event: Array<any>) {
       const elementRefInPath = $event.find(node => node === this.parentNode);
       if (!elementRefInPath) {
         this.closeEventEmmit.emit(this.filterState);
        //  this.filtersEventEmmit.emit(this.filterState);
       }
     }

     closeMe($event: any){
      this.closeEventEmmit.emit(this.filterState);
     }

     setFilterState(filterState: FilterState, name: string){
       this.filterState = filterState;
       this.myFilterList = filterState[name].filterList;
       this.AtoZChecked = filterState[name].AtoZ;
       this.ZtoAChecked = filterState[name].ZtoA;
       this.columnName = name;
     }

     checkboxChecked(event: any){
      var target = event.target || event.srcElement || event.currentTarget;
      this.myFilterList = this.myFilterList.map( f => { 
        if(f.name === target.parentElement.innerText.trim()){
          f.isChecked = !f.isChecked;
        } 
        return f;
      });
      
      let markAsDirty = false;
        this.myFilterList.map(f => {
          if(f.isChecked) { markAsDirty = true; }
        })

          this.filterState[this.columnName].isDirty = markAsDirty;
     }

     radioClick(event: any){
      var target = event.target || event.srcElement || event.currentTarget;
      var idAttribute = target.id;
      this.filterState[this.columnName].isDirty = true;
      if(idAttribute && idAttribute == 'AtoZ'){
          this.filterState[this.columnName].AtoZ = true;
          this.filterState[this.columnName].ZtoA = false;
          // <HTMLInputElement>(document.getElementById("ZtoA")).checked = false;
          this.ZtoAChecked = false;

          return;
       }

       if(idAttribute && idAttribute == 'ZtoA'){
          this.filterState[this.columnName].ZtoA = true;
          this.filterState[this.columnName].AtoZ = false;
          // this.filterState.AtoZ = false;
          this.AtoZChecked = false;
       }
    }
  }