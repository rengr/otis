﻿import { Component} from '@angular/core';
// import { state, animate } from '@angular/animate';
import { RouterModule } from '@angular/router';
import { UserProfileService } from '../Services/userprofile.service';
declare var BUILD_DATE: string;
// declare var LogRocket : any;
@Component({
    selector: 'sidebar',

    // animations: [
    //     trigger('openClose', [
    //         state('false', style({
    //             width: '50px',
    //         })),
    //         state('true', style({
    //             width: '200px',
    //         })),
    //         transition('true => false', animate('100ms ease-in')),
    //         transition('false => true', animate('100ms ease-out'))
    //     ])
    // ],
    templateUrl: './sidebar.component.html',
})
export class SidebarComponent {

    public isMenuOpen: string = 'true';
    public myBuildDate: string = BUILD_DATE;
    public userName: string = '';

    constructor(public router: RouterModule, public userProfile : UserProfileService) {
        userProfile.userProfile$.subscribe((user: any) => {
            if(user.UserName){
                this.userName = user.UserName;
                // LogRocket.identify('OTiS', {
                //     name: user.UserName
                //     // email: 'jamesmorrison@example.com',
                  
                //     // // Add your own custom user variables here, ie:
                //     // subscriptionType: 'pro'
                //   });
            }
        });
        // userProfile.getUserProfile();
    }

    public menuOpen() {
        //this.isMenuOpen = !this.isMenuOpen;
        if (this.isMenuOpen === 'true') {
            this.isMenuOpen = 'false';
        }
        else {
            this.isMenuOpen = 'true';
        }
        
    }
}