﻿import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({ name: 'dateformatshort' })
export class DateFormatShortPipe implements PipeTransform {
    transform(value: string, args: string[]): any {
        if (!value) return value;

        let t;

        if (args) {
            //var timeZoneData = args[0];
            t = moment.utc(value, moment.ISO_8601).subtract(360, 'm').format('MM/DD/YYYY h:mm A');
        }
        else {
            t = moment(value, moment.ISO_8601).format('MM/DD/YYYY h:mm A');
        }

        return t;
        //return value.replace(/\w\S*/g, function (txt) {
        //    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        //});
    }
}