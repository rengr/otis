﻿import { NotificationService } from '../Services/notification.service';
import { Component, OnInit } from '@angular/core';
import { RouterModule } from '@angular/router';

@Component({
    selector: 'navbar',
    templateUrl: './navbar.component.html'
})

export class NavbarComponent implements OnInit {
    constructor(private notification: NotificationService){

    }
       public isCollapsed: boolean = true;
       private message : string = '';
       private msgType: number = 1;
       ngOnInit(){
       this.notification.notification$.subscribe(
                data => { // data is null on the follow-up emission
                    this.message = data?data.message:''; 
                    this.msgType = data?data.type:1;
                },
                error => console.log('send this error to the error service', error),
                () => console.log('completed deleted')
        )
    }

    public collapsed(event: any): void {
        console.log(event);
    }

    public expanded(event: any): void {
        console.log(event);
    }

    logout() {
        window.location.href = '/Logout';
        sessionStorage.clear();
    }
}