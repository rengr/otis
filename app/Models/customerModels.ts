export class Customer {
    /* Required */
    organizationId: number = 0;
    name: string = null;
    shortName: string = null;
    /* Required */
    partnerId: number = null;
    ftpUserName: string = null;
    ftpPassword: string = null;
    backendUserName: string = null;
    backendPassword: string = null;
    shippingAccount: string = null;
    notes: string = null;
    templateNotes: string = null;
    projectList: string[] = null;
}
