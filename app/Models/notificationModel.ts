export enum NotificationType{
    error = 0, 
    info = 1,
    warning = 2
}

export class Notification{
    message: string;
    type: NotificationType;

    constructor(init?:Partial<Notification>){
        Object.assign(this, init);
    }
}