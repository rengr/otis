export enum OrderStatusEnum{
    Cancelled   = 1,
    Pending     = 2,
    Approved    = 3,
    Shipped     = 4,
    Returned    = 5,
    BackedUp    = 6,
    ExportReady = 7,
    Complete    = 8,
    Report_Complete = 9,
    AppDownload = 10,
    ListImport  = 11
}

export const OrderStatusDescriptions: {[key in OrderStatusEnum]: string} = {
    [OrderStatusEnum.Cancelled]: 'Cancelled',
    [OrderStatusEnum.Pending]: 'Pending',
    [OrderStatusEnum.Approved]: 'Approved',
    [OrderStatusEnum.Shipped]: 'Shipped',
    [OrderStatusEnum.Returned]: 'Returned',
    [OrderStatusEnum.BackedUp]: 'Backed Up',
    [OrderStatusEnum.ExportReady]: 'Export Ready',
    [OrderStatusEnum.Complete]: 'Complete',
    [OrderStatusEnum.Report_Complete]: 'Report Complete',
    [OrderStatusEnum.AppDownload]: 'App Download',
    [OrderStatusEnum.ListImport]: 'List Import'
}

export enum BadgeTypeEnum{
    None        = 0,
    ManualEntry = 1,
    PDF417      = 2,
    OneD        = 3,
    QR          = 4,
    Magstripe   = 5,
    RFID        = 6,
    Aztec       = 7,
    OCR         = 8
}
export enum BadgeOrderStatusEnum{
    Purchase          = 1,
    DoNotPurchase     = 2,
    Pending           = 3,
    Investigate       = 4,
    AcquiredForOther  = 5,
    Purchased         = 6,
    ResearchOnly      = 7,
    ExhibitorOrdering = 8,
    NotAvailable      = 9,
    Researched        = 10,
}
export enum BadgingServiceEnum{
    NA        = 1,
    RealTime  = 2,
    PostMatch = 3,
    BadgeScan = 4,
    ImageCapture = 5,
    ListMatch = 6,
    Import = 7,
    ManualEntry = 8,
    Other = 9
}
export enum ShipMethodEnum{
    HandCarry = 1,
    FirstOvernight = 2,
    Overnight = 3,
    TwoDay = 4,
    ThreeDayExpress = 5,
    Ground = 6,
    Other = 7,
    TwoDayAM = 8,
    NotShipping = 9,
}

export enum OrderSupportTypeEnum{
    Approved = 1,
    Cancelled = 2,
    Pending = 3,
    // Approved = 4,
    // FreemanAV = 5
}

export class Order {
    /* Required */
    orderId: number = 0;
    description: string = null;
    accountOwner: string = null;
    organizationId: number = null;
    organizationShortName: string = null;
    beginDate: string = null;
    projectDueDate: string = null;
    status: OrderStatusEnum = null;
    statusDescription: string = null;
    supportBadgeRequired: boolean = false;
    hasCompliance: boolean = false;
    /* Required */
    quoteNumber: number = null;
    accountManager: string = null;
    projectLead: string = null;
    orderDate: string = null;
    endDate: string = null;
    shipDate: string = null;
    arrivalDate: string = null;
    equipmentReturned:  boolean = false;
    postShowDeliveryDate: string = null;
    location: string = null;
    badgeType: string = null;
    badgingCompany: string = null;
    badgeOrderStatus: string = null;
    badgingService: string = null;
    badgeKitCost: string = null;
    badgeKitOrderDate: string = null;
    badgeKitReceived: boolean = false;
    badgeFormatAddedBy: string = null;
    badgeNotes: string = null;
    customerContactId: number = null;
    customerContactName: string = null;
    jobNumber: string = null;
    website: string = null;
    boothNumber: string = null;
    venue: string = null;
    notes: string = null;
    hours: string = null;
    shipMethod: ShipMethodEnum = null;
    shipResponsibility: string = null;
    shippingAddress: string = null;
    warehouseStartDate: string = null;
    warehouseTarget: string = null;
    warehouseDeadline: string = null;
    trackingNumbers: string = null;
    returnTrackingNumbers: string = null;
    shippingNotes: string = null;
    support: string = null;
    supportTechs: {}[] = [];
    supportDateIn: string = null;
    supportDateOut: string = null;
    supportHotelClientProvided: boolean = false;
    supportFlightBooked: boolean = false;
    supportHotelBooked: boolean = false;
    supportTravelDateIn: string = null;
    supportTravelDateOut: string = null;
    supportHotelAddress: string = null;
    onSiteContactId: number = null;
    supportNotes: string = null;
    billingAddress: string = null;
    billingEmail: string = null;
    billingPhone: string = null;
    billingCell: string = null;
    billingNotes: string = null;
    complianceInfo: string = null;
}


export interface OrderListItem {
    orderId: number;
}

export class OrderListItemGeneral extends Order {
    /* Required */
    orderId: number = 0;
    statusDescription: string = null;
    description: string = null;
    organizationShortName: string = null;
    beginDate: string = null;
    endDate: string = null;
    accountManager: string = null;
    projectDueDate: string = null;
    accountOwner: string = null; 
    quoteNumber: number = null;
    postShowDeliveryDate: string = null; 
    // organizationId: number = null; // drop?
    /* Required */
    // orderDate: string = null; // drop?
    // shipDate: string = null; 
    // arrivalDate: string = null; // drop?
    // // equipmentReturned: boolean = false; // drop?

    // badgeKitOrderDate: string = null;
    // badgeKitReceived: boolean = false;
    // badgingCompany: string = null;

    // // jobNumber: string = null;

    // /* Descriptor Fields */
    // badgeOrderStatusDescription: string = null;
    // badgingServiceDescription: string = null;
    // badgeTypeDescription: string = null;
    
    // supportTech: string = null;
    // supportDateIn: string = null;
    // supportDateOut: string = null;
    // supportTravelDateIn: string = null;
    // supportTravelDateOut: string = null;
    // supportHotelClientProvided: boolean = false;
    // supportHotelAddress: string = null;
    // onsiteContactId: number = 0;
    // supportNotes: string = null;
}

export class OrderListItemBadges extends Order {
    /* Required */
    orderId: number = 0;
    statusDescription: string = null;
    description: string = null;
    organizationShortName: string = null;
    beginDate: string = null;
    badgeOrderStatusDescription: string = null;
    badgeKitOrderDate: string = null;
    badgeKitReceived: boolean = false;
    badgingCompany: string = null;
    badgingServiceDescription: string = null;
    badgeTypeDescription: string = null;


    // accountOwner: string = null; 
    // // organizationId: number = null; // drop?
    // projectDueDate: string = null;
    // /* Required */
    // accountManager: string = null;
    // // orderDate: string = null; // drop?
    // endDate: string = null;
    // shipDate: string = null; 
    // arrivalDate: string = null; // drop?
    // // equipmentReturned: boolean = false; // drop?
    // postShowDeliveryDate: string = null; 



    // // jobNumber: string = null;
    // quoteNumber: number = null;

    // /* Descriptor Fields */
    
    // supportTech: string = null;
    // supportDateIn: string = null;
    // supportDateOut: string = null;
    // supportTravelDateIn: string = null;
    // supportTravelDateOut: string = null;
    // supportHotelClientProvided: boolean = false;
    // supportHotelAddress: string = null;
    // onsiteContactId: number = 0;
    // supportNotes: string = null;
}
export class OrderListItemShipping extends Order {
    /* Required */
    orderId: number = 0;
    statusDescription: string = null;
    description: string = null;
    organizationShortName: string = null;
    shipDate: string = null; 
    arrivalDate: string = null; // drop?
    beginDate: string = null;
    accountOwner: string = null; 
    accountManager: string = null;
    shipMethod: number = null;
      



    // // organizationId: number = null; // drop?
    // projectDueDate: string = null;
    // /* Required */
    // // orderDate: string = null; // drop?
    // endDate: string = null;
    // // equipmentReturned: boolean = false; // drop?
    // postShowDeliveryDate: string = null; 

    // badgeKitOrderDate: string = null;
    // badgeKitReceived: boolean = false;
    // badgingCompany: string = null;

    // // jobNumber: string = null;
    // quoteNumber: number = null;

    // /* Descriptor Fields */
    // badgeOrderStatusDescription: string = null;
    // badgingServiceDescription: string = null;
    // badgeTypeDescription: string = null;
    
    // supportTech: string = null;
    // supportDateIn: string = null;
    // supportDateOut: string = null;
    // supportTravelDateIn: string = null;
    // supportTravelDateOut: string = null;
    // supportHotelClientProvided: boolean = false;
    // supportHotelAddress: string = null;
    // onsiteContactId: number = 0;
    // supportNotes: string = null;
}
export class OrderListItemSupport extends Order {
    /* Required */
    orderId: number = 0;
    organizationShortName: string = null;
    support: string = null;
    beginDate: string = null;
    endDate: string = null;
    description: string = null;
    supportTech: {} = {};
    supportDateIn: string = null;
    supportDateOut: string = null;
    supportTravelDateIn: string = null;
    supportTravelDateOut: string = null;
    location: string = null;
    // supportHotelClientProvided: boolean = false;
    // supportHotelAddress: string = null;
    // onsiteContactId: number = 0;



    // supportNotes: string = null;
    // accountOwner: string = null; 
    // // organizationId: number = null; // drop?
    // projectDueDate: string = null;
    // /* Required */
    // accountManager: string = null;
    // // orderDate: string = null; // drop?
    // endDate: string = null;
    // shipDate: string = null; 
    // arrivalDate: string = null; // drop?
    // // equipmentReturned: boolean = false; // drop?
    // postShowDeliveryDate: string = null; 
    // organizationShortName: string = null;

    // badgeKitOrderDate: string = null;
    // badgeKitReceived: boolean = false;
    // badgingCompany: string = null;

    // // jobNumber: string = null;
    // quoteNumber: number = null;

    // /* Descriptor Fields */
    // statusDescription: string = null;
    // badgeOrderStatusDescription: string = null;
    // badgingServiceDescription: string = null;
    // badgeTypeDescription: string = null;
    
}

export const GeneralListHeaders = [
    { displayName:"Status",      idName:"statusDescription",     sortDirection:1, fieldName:'statusDescription', },
    { displayName:"Description", idName:"description",           sortDirection:1, fieldName:'description'},
    { displayName:"Customer",    idName:"organizationShortName", sortDirection:1, fieldName:'organizationShortName'},
    { displayName:"Begin Date" , idName:"beginDate",             sortDirection:1, fieldName:'beginDate'},
    { displayName:"End Date" ,   idName:"endDate",               sortDirection:1, fieldName:'endDate'},
    { displayName:"Acct Mgr",    idName:"accountManager",        sortDirection:1, fieldName:'accountManager'},
    { displayName:"Due Date" ,   idName:"projectDueDate",        sortDirection:1, fieldName:'projectDueDate'},
    { displayName:"Acct Owner",  idName:"accountOwner",          sortDirection:1, fieldName:'accountOwner'},
    { displayName:"Quote #",     idName:"quoteNumber",           sortDirection:1, fieldName:'quoteNumber'},
    { displayName:"Post Report", idName:"postShowDeliveryDate",  sortDirection:1, fieldName:'postShowDeliveryDate'},
];
export const badgeListHeaders = [
    { displayName:"Status",           idName:"statusDescription",           sortDirection:1, fieldName:'statusDescription'},
    { displayName:"Description",      idName:"description",                 sortDirection:1, fieldName:'description'},
    { displayName:"Customer",         idName:"organizationShortName",       sortDirection:1, fieldName:'organizationShortName'},
    { displayName:"Begin Date" ,      idName:"beginDate",                   sortDirection:1, fieldName:'beginDate'},
    { displayName:"Kit Order Status", idName:"badgeOrderStatus",            sortDirection:1, fieldName:'badgeOrderStatus'},
    { displayName:"Kit Ordered On",   idName:"badgeKitOrderDate",           sortDirection:1, fieldName:'badgeKitOrderDate'},
    { displayName:"Received",         idName:"badgeKitReceived",            sortDirection:1, fieldName:'badgeKitReceived'},
    { displayName:"Badging Company",  idName:"badgingCompany",              sortDirection:1, fieldName:'badgingCompany'},
    { displayName:"Badge Services",   idName:"badgingService",              sortDirection:1, fieldName:'badgingService'},
    { displayName:"Ship Date",        idName:"shipDate",                    sortDirection:1, fieldName:'shipDate'},
];
export const shippingListHeaders = [
    { displayName:"Ship Date",    idName:"shipDate",              sortDirection:1, fieldName:'shipDate'},
    { displayName:"Status",       idName:"statusDescription",     sortDirection:1, fieldName:'statusDescription', },
    { displayName:"Description",  idName:"description",           sortDirection:1, fieldName:'description'},
    { displayName:"Customer",     idName:"organizationShortName", sortDirection:1, fieldName:'organizationShortName'},
    { displayName:"Delivery Date",idName:"arrivalDate",           sortDirection:1, fieldName:'arrivalDate'},
    { displayName:"Begin Date",   idName:"beginDate",             sortDirection:1, fieldName:'beginDate'},
    { displayName:"Acct Owner",   idName:"accountOwner",          sortDirection:1, fieldName:'accountOwner'},
    { displayName:"Acct Mgr",     idName:"accountManager",        sortDirection:1, fieldName:'accountManager'},
];

export const supportListHeaders = [
    // { displayName:"Status",       idName:"statusDescription",           sortDirection:1, fieldName:'statusDescription', },
    { displayName:"Description",  idName:"description",                 sortDirection:1, fieldName:'description'},
    { displayName:"Customer",     idName:"organizationShortName",       sortDirection:1, fieldName:'organizationShortName'},
    { displayName:"Support Status", idName:"support",           sortDirection:1, fieldName:'support', },
    { displayName:"Tech",         idName:"supportTech",                 sortDirection:1, fieldName:'supportTech'},
    { displayName:"Travel In",    idName:"supportTravelDateIn",         sortDirection:1, fieldName:'supportTravelDateIn'},
    { displayName:"Support Setup",      idName:"supportDateIn",               sortDirection:1, fieldName:'supportDateIn'},
    { displayName:"Show Start" ,  idName:"beginDate",                   sortDirection:1, fieldName:'beginDate'},
    { displayName:"Show End",     idName:"endDate",                     sortDirection:1, fieldName:'endDate'},
    // { displayName:"Date Out",     idName:"supportDateOut",              sortDirection:1, fieldName:'supportDateOut'},
    { displayName:"Travel Out",   idName:"supportTravelDateOut",        sortDirection:1, fieldName:'supportTravelDateOut'},
    { displayName:"Location",     idName:"location",                    sortDirection:1, fieldName:'location'},
    // { displayName:"Hotel Provided",idName:"supportHotelClientProvided", sortDirection:1, fieldName:'supportHotelClilentProvided'},
    // { displayName:"Hotel Address" ,idName:"supportHotelAddress",        sortDirection:1, fieldName:'supportHotelAddress'},
   // { displayName:"Site Contact", idName:"onsiteContactId",             sortDirection:1, fieldName:'onsiteContactId'},
    // { displayName:"Notes" ,       idName:"supportNotes",                sortDirection:1, fieldName:'supportNotes'},
];