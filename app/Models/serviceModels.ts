export class Service {
  /* Required */
  serviceId: number = 0;
  description: string = null;
  name: string = null;
  /* Required */
}
export class ServiceOption {
  /* Required */
  optionId: number = 0;
  serviceId: number = 0;
  description: string = null;
  name: string = null;
  /* Required */
  uiIsSelected: boolean = false;
}
