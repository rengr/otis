export class Employee {
  userName: string;
  displayName: string;
  type: number[];
}

export enum EmployeeType{
  developer = 1,
  sales = 2,
  support = 3,
  operations = 4
}