import * as moment from 'moment';

export enum QuoteStatus {
    Quoted = 1,
    Estimate = 2,
    Pending = 3,
    Approved = 4,
    NotApproved = 5,
    Canceled = 6,
    Replaced = 7
}
export class Quote {
    /* Required */
    quoteNumber: number = 0;
    /* Required */
    projectTitle: string = null;
    salesPerson: string = null;
    organizationId?: number = null;
    contactId?: number = null;
    companyName: string = null;
    contactName: string = null;
    quotationDate: string = null;
    businessType: string = null;
    status: QuoteStatus = null;
    billable: boolean = false;

    organizationDisplayName: string = null;
    contactDisplayName: string = null;

    get statusDisplay(): string {
        return QuoteStatus[this.status];
    }
    getQuatationDateAsMoment(): moment.Moment {
        if (this.quotationDate) {
            return moment(this.quotationDate);
        }
        return null;
    }
}
