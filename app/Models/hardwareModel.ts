export class HardwareListItem {
  productId: number = 0;
  description: string = '';
}
export class HardwareRentalItem {
  /* Rquired */
  itemId: number = 0;
  rentalProductId: number = 0;
  quantity: number = 0;
  backupDevices: number = 0;
  orderId: number = 0;
  /* Rquired */
  orderItemId: number = null;
  notes: string = null;
  productDescription: string = null;
}
