﻿/* Defines Events for each business entity (project)*/
export interface IEvent {
    BeginDate: string;
    EndDate: string;
    EventId: number;
    EventName: string;
    HasRegistrationProvider: string;
    TimeZone: string;
}

export class Event implements IEvent {
    BeginDate: string;
    EndDate: string;
    EventId: number;
    EventName: string;
    HasRegistrationProvider: string;
    TimeZone: string;
}