import { Service,ServiceOption } from './serviceModels';

export function IdListItemConverter(value?: any): any {
  if(value !== undefined) {
    var result = new IdListItem();
    if(value.Id !== undefined)
      result.id = value.Id;
    if(value.Name !== undefined)
      result.name = value.Name;
    if(value.Delete !== undefined)
      result.delete = value.Delete;
    return result;
  }
  else { return null; }
}
export class IdListItem {
  id: number = 0;
  name: string = null;
  delete: boolean = false;
}
export class OrderItem {
  /* Required */
  itemId: number = 0;
  orderId: number = 0;
  /* Required */
  serviceId: number = null;
  notes: string = null;
  serviceOptionIds: Array<IdListItem> = [];
  // Display only fields
  serviceDescription: string;
  serviceName: string;
  sericeOptionsDisplay: string;

  updateServiceDescription(serviceList: Array<Service>) {
    // if(this.serviceId == null) {
    //   this.serviceDescription = "";
    // } else {
    //   var myService = serviceList.find( (srv) => srv.serviceId == this.serviceId );
    //   if( myService != undefined ) {
    //     this.serviceName = myService.description;
    //   }
    // }
  }
  updateOptionsDisplay(serviceOptionsList: Array<ServiceOption>) {
    var myOptions: Array<string>= [];
    serviceOptionsList.forEach( srvOpt => {
      if( this.serviceOptionIds.find(optId => optId.id == srvOpt.optionId) != undefined ) {
        myOptions.push(srvOpt.description)
      }
    });
    this.sericeOptionsDisplay = myOptions.join(', ');
  }
}
