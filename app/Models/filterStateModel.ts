export class filterListItem {
    name: string;
    isChecked: boolean;
}

export class ColumnState {
    columnName: string;
    AtoZ: boolean;
    ZtoA: boolean;
    filterList: filterListItem[];
    isDirty: boolean;
}

export class FilterState {
    // columns: ColumnState[];
    
}