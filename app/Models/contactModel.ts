import { IdListItem } from "./orderItemModel";

export class Contact{
    contactId: number = 0;
    partnerId: number = null;
    firstName: string = null;
    lastName: string = null;
    company: string = null;
    email: string = null;
    jobTitle: string = null;
    workPhone: string = null;
    mobilePhone: string = null;
    notes: string = null;
    /* Descriptor and Mapped Fields */
    partnerName: string = null;
    linkedOrganizations: IdListItem[] = null;
}