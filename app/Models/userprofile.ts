﻿export class User {
    UserName: string;
    MasterUserId: number;
    Email: string;
    FirstName: string;
    LastName: string;
    Organization: string;
    Address1: string;
    Address2: string;
    City: string;
    StateOrRegion: string;
    PostalCode: string;
    CountryCode: string;
    Phone: number;
    Fax: number;
    ETag: string;
    Roles: Array<string>;
    Active: boolean;
    IsLockedOut: boolean;
    EmailName: string;
}