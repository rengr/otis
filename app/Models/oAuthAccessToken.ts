
import * as moment from 'moment';

export class AccessToken {
    access_token: string = null;
    expires_in: string = null;
    token_type: string = null;
    expiresAsMoment(): moment.Moment {
        if (!this.expires_in) { return null; }
        return moment(this.expires_in);
    }
}

// export class AuthBundle {
//     accessToken: AccessToken;
//     refreshToken: string;
// }

//  This doesnt work as a bundle because when we refresh access token we dont have refresh token, so it gets overwritten
//  export class AuthBundle {
//     accessToken: AccessToken;
//     refreshToken: string;
// }