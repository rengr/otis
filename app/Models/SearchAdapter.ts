import { Observable ,  Subscription } from "rxjs";

export class ResultItem {
  id: any;
  text: string;
}
export class SearchAdapter<reqType> {
  private mySubscription: Subscription;
  /**
   *
   */
  constructor(
    request$: Observable<reqType>, mapper: (data:any) => ResultItem, successCallback: any, failureCallback: any, includeNoneOption: boolean = true) {
    this.mySubscription = request$.subscribe(
      (data:reqType) => {
        let arrayLike = <any>data;
        let resArray = [];
        if(arrayLike) {
          arrayLike.forEach(function(rawResult){
            if(rawResult) { resArray.push(mapper(rawResult));}
          });
        }
        resArray.sort(function(lhs,rhs) {
          if(lhs.text <  rhs.text) { return -1; }
          if(lhs.text == rhs.text) { return 0; }
          return 1;
        });
        if(includeNoneOption) {
          resArray.splice(0,0,{id: '', text:'-- None --'});
        }
        successCallback({ results: resArray });
      },
      err => failureCallback(err));
  }
  abort() {
    if(this.mySubscription != null) {
      this.mySubscription.unsubscribe();
      this.mySubscription = null;
    }
  }
}