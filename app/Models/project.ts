﻿/* Defines Projects (Business Objects, Bussiness Entities)*/
export class Project {
    projectcode: string;
    ProjectId: string;
    ProjectName: string;
    userId: number;
}