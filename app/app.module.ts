﻿// base, core, tools, infrastructure
import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { routing } from './app.routes';
import { AppComponent }  from './app.component';
import { RouterModule } from '@angular/router';
import { CommonModule, HashLocationStrategy, LocationStrategy } from '@angular/common';
import { Select2Component } from 'angular-select2-component';

// components
import { CustomersComponent } from './Customers/customers.component';
import { ContactsComponent } from './Contacts/contacts.component';
import { OrdersComponent } from './Orders/orders.component';
import { QuotesComponent } from './Quotes/quotes.component';
import { NavbarComponent } from './Chrome/navbar.component';
import { SidebarComponent } from './Chrome/sidebar.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpModule} from '@angular/http';
import { LoggedOutComponent } from './Services/OAuth/loggedout.component';
import { SigninComponent } from './Signin/signin.component'
import { HardwareComponent } from './Hardware/hardware.component';
import { PartnersComponent } from './Partners/partners.component';

import { QuoteDetailsComponent } from './Quotes/quoteDetails.component';
import { CustomerDetailsComponent } from './Customers/customerDetails.component';
import { ContactDetailsComponent } from './Contacts/contactDetails.component';
import { OrderDetailsComponent } from './Orders/orderDetails.component';
import { OrderItemsComponent } from './Orders/orderItems.component';
import { PopOverComponent } from  './Chrome/popover.component';
//external modules 
// services
import { AuthGuardService } from './Services/OAuth/authguard.service';
import { DirtyFormGuardService } from './Services/dirtyFormGuard.service';
import { DialogService } from './Services/dialog.service';
import { HardwareService } from './Services/hardware.service';
import { UserProfileService} from './Services/userprofile.service';
import { BaseUrlService } from './Services/baseurl.service';
//import { HttpService } from './Services/http.service';
import { OAuthResourceService } from './Services/OAuth/OAuthResource.service';
import { QuoteApiService } from './Services/quotesApi.service';
import { CustomerApiService } from './Services/customerApi.service';
import { ContactApiService } from './Services/contactsApi.service';
import { ServiceApiService } from './Services/serviceApi.service';
import { NotificationService } from './Services/notification.service';
import { LogService } from './Services/log.service';
import { OrderApiService } from './Services/orderApi.service';
import { UtilityService } from './Services/utilities';
import { EmployeeService }  from './Services/employees.service';
import { SearchService } from './Services/search.service';
import { PartnerApiService } from  './Services/partnerApi.service';
import { PopOverService } from  './Services/popover.service';

// interceptors
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './Services/OAuth/token.interceptor';
// pipes
import { DateFormatShortPipe } from './Chrome/dateformat.pipe'; 

@NgModule({
    imports: [RouterModule, routing, BrowserModule, HttpClientModule, HttpModule, CommonModule, FormsModule, ReactiveFormsModule],       // module dependencies
    declarations: [AppComponent, Select2Component, ContactsComponent,
        OrdersComponent, CustomersComponent,  QuotesComponent, NavbarComponent,
        SidebarComponent, DateFormatShortPipe, SigninComponent, LoggedOutComponent,
        QuoteDetailsComponent, CustomerDetailsComponent, OrderDetailsComponent, 
        ContactDetailsComponent, OrderItemsComponent, HardwareComponent, PartnersComponent, PopOverComponent],   // components and directives
    bootstrap: [AppComponent],     // root component
    //providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy }]
    providers: [UserProfileService, BaseUrlService, AuthGuardService, DirtyFormGuardService, DialogService, OAuthResourceService,
                QuoteApiService, CustomerApiService, OrderApiService, ServiceApiService, ContactApiService,
                NotificationService, LogService, UtilityService, EmployeeService, SearchService, HardwareService,
                PartnerApiService, PopOverService,
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: TokenInterceptor,
                    multi: true
                }],
    entryComponents: [PopOverComponent]    
})
export class AppModule { }

// the rain

// in spain

// falls mainly down the drain

