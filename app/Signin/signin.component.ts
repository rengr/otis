import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { OAuthResourceService } from '../Services/OAuth/OAuthResource.service';
import { Subject } from 'rxjs';
import { catchError } from 'rxjs/operators';
class Credentials {
    username: string;
    password: string;
}

@Component({
    templateUrl: './signin.component.html'
})
export class SigninComponent {
    private login$: Subject<Credentials>;

    public message: string = 'You might be logged in';
    public usernameBox: string;
    public passwordBox: string;

    constructor(public router: Router, private authService: OAuthResourceService) {
        this.message = 'You are not logged in';

        this.login$ = new Subject<Credentials>();
        this.login$.asObservable()
            .subscribe(creds => {
                authService.doLogin(creds.username, creds.password)
                    .pipe(
                        catchError((err, caught) => { 
                            // debugger;
                            this.message = err;
                            return caught;
                        })
                    )
                    .subscribe(_ => { 
                        router.navigate(['home'])
                    });
            });

    }

    doLogin() {
        this.login$.next({ username: this.usernameBox, password: this.passwordBox });
    }

}