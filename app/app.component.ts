﻿﻿import { Component, ViewChild} from '@angular/core'
import { SidebarComponent } from './Chrome/sidebar.component';

@Component({
    selector: 'ng-app',
    styles: [`
        :host {
          display: block !important;
        }
    `],

    templateUrl: './Chrome/chrome.component.html',
})


export class AppComponent {
    @ViewChild('mysidebar') sidebar: SidebarComponent; 
    isToggled: boolean = false;

    public menuOpen() {
        this.sidebar.menuOpen();
        this.isToggled = !this.isToggled;
    }
}