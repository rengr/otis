import { Injectable } from '@angular/core';
import { Observable, of as Observableof } from 'rxjs';

@Injectable()
export class DialogService {
  confirm(message?: string): Observable<boolean> {
    const confirmation = window.confirm(message || 'Are you sure?');

    return Observableof(confirmation);
  };
}