
import { of as Observableof, throwError as observableThrowError, Observable, BehaviorSubject } from 'rxjs';
import { catchError, map, filter } from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { Http, Headers, Response, RequestOptions } from "@angular/http";
import { AccessToken } from '../../Models/oAuthAccessToken';
import * as moment from 'moment';


@Injectable()
export class OAuthResourceService {
    private oAuthTokenUrl: string = 'https://sso.renventsuite.com/oauth/token';
    private oAuthUserUrl: string = 'https://wms.rengr.co/api/account';
    private ClientSec: string = 'MTMxNTk0OTg3NzE5MDM2OTg0Og==';
    private accessToken: AccessToken = null;
    private refreshToken: string = null;
    private userInfo: any = null;

    private isAuthedObservable: Observable<boolean>;
    private isAuthedObserver: BehaviorSubject<boolean>;

    constructor(private http: Http) {
        this.isAuthedObserver = new BehaviorSubject(false);
        this.isAuthedObservable = this.isAuthedObserver.asObservable();

        // Load token data from storage, if supported
        if (this.supportsLocalStorage()) {
            let storageAuthBundle = JSON.parse(localStorage.getItem('atbundle'));
            if (storageAuthBundle) {
                this.accessToken = storageAuthBundle;
                this.isAuthedObserver.next(true);
            }
            else{
                this.isAuthedObserver.next(false);
            }

            this.refreshToken = JSON.parse(localStorage.getItem('f'));
        }
    }

    public get $isAuthenticatedObservable() {
        return this.isAuthedObservable;
    }

    private supportsLocalStorage(): boolean {
        if (typeof (Storage) !== 'undefined') {

            var test = "__localstoragetest__";
            try {
                window.localStorage.setItem(test, test);
                window.localStorage.removeItem(test);
            } catch (ex) {
                return false;
            }

            return true;
        }

        return false;
    }

    public saveToken(returnedBody: any) {
        let resp = JSON.parse(returnedBody);
        let expireDate = moment().add('seconds', resp.expires_in);
        let newBundle = new AccessToken();
        // newBundle['refreshToken'] = resp.refresh_token;
        // newBundle = new AccessToken();
        newBundle = resp;
        // newBundle['accessToken'].expires = expireDate.toISOString();

        if (this.supportsLocalStorage()) {
            localStorage.setItem('atbundle', JSON.stringify(newBundle));
            localStorage.getItem('atbundle');

            if(resp.refresh_token){
                localStorage.setItem('f', JSON.stringify(resp.refresh_token));
                localStorage.getItem('f');
            }
        }

        this.accessToken = newBundle;
        if (resp.refresh_token) {
            this.refreshToken = resp.refresh_token;
        }
        this.isAuthedObserver.next(true);

        // this.authBundleObserver.next(newBundle);
        return newBundle;
    }

    public doLogin(username: string, password: string) {
        let headers = new Headers();
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        headers.append('Authorization', 'Basic ' + this.ClientSec);
        let options = new RequestOptions({ headers: headers });
        let body = 'grant_type=password&username=' + username + '&password=' + password;
        return this.http.post(this.oAuthTokenUrl, body, options)
            .pipe(
                catchError(error => {
                    if (error.status == 400 || error.status == 401 || error.status == 403) {
                        // this.isAuthenticatedBeingObserved.next(false);
                        // debugger;
                        return observableThrowError(error);
                    }
                }),
                map((response: Response) => {
                    let resp = <any>response;
                    this.saveToken(resp._body);
                    // return true;
                })
             );
    }

    // not Sure we are doing a logout
    public doLogout() {
        this.isAuthedObserver.next(false);
    }

    public getToken(): Observable<AccessToken> {
        if (!this.accessToken || moment(this.accessToken.expires_in) < moment().add('seconds', 10)) {
            return this.refreshAccessToken();
        }
        return Observableof(this.accessToken);
    }
    
    public refreshAccessToken(): Observable<AccessToken> {
        // console.log('your token is old and nasty ');
        let headers = new Headers();
        headers.append('Accept', 'application/json');
        headers.append('Authorization', 'Basic ' + this.ClientSec);
        let options = new RequestOptions({ headers: headers });
        let imaToken = this.refreshToken; // ? this.refreshToken : this.accessToken.refreshToken;
        let body = 'grant_type=refresh_token&refresh_token=' + imaToken;  //this.refreshTokenObserver.value;
        return this.http.post(this.oAuthTokenUrl, body, options)
            .pipe(
                map((response: Response) => {
                    let resp = <any>response;
                    let accessToken: AccessToken = this.saveToken(resp._body);
                    return accessToken;
                })
            );
    }
    public getUserInfo() {
        return this.userInfo;
    }

    public getUserName() {
        // return this.userInfo ? this.userInfo[this.oAuthUserNameField] : null;
    }

    // private fetchUserInfo() {
    //     if (this.token != null) {
    //         var headers = new Headers();
    //         headers.append('Authorization', `Bearer ${this.token}`);
    //         headers.append('Accept', 'application/json');
    //         //noinspection TypeScriptUnresolvedFunction
    //         this.http.get(this.oAuthUserUrl, { headers: headers })
    //             .map(res => res.json())
    //             .subscribe(info => {
    //                 this.userInfo = info;
    //             }, err => {
    //                 console.error("Failed to fetch user info:", err);
    //             });
    //     }
    // }
}

