import { OAuthResourceService } from './OAuthResource.service';
import { of, throwError as observableThrowError, EMPTY, Observable, BehaviorSubject, onErrorResumeNext } from 'rxjs';
import { mergeMap, take, filter, catchError, switchMap, finalize } from 'rxjs/operators';
import { Injectable, Injector } from "@angular/core";
import { HttpInterceptor, HttpEvent, HttpRequest, HttpHandler, HttpSentEvent, HttpHeaderResponse, HttpProgressEvent, HttpResponse, HttpUserEvent, HttpErrorResponse } from "@angular/common/http";
import { AccessToken } from '../../Models/oAuthAccessToken';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    isRefreshingToken: boolean = false;
    // tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);

    constructor(private injector: Injector, private oAuth: OAuthResourceService) { }

    addToken(req: HttpRequest<any>, token: string): HttpRequest<any> {
        return req.clone({ setHeaders: { Authorization: 'Bearer ' + token } })
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return this.oAuth.getToken()
            .pipe(
                catchError(() => {
                    return this.logoutUser();
                }),
                mergeMap((accessToken: AccessToken) => {
                    if (accessToken) {
                        var newReq = this.addToken(req, accessToken.access_token);
                    }
                    return next.handle(newReq)
                        .pipe(
                            catchError((error, caught) => {
                                if (error instanceof HttpErrorResponse) {
                                    switch ((<HttpErrorResponse>error).status) {
                                        case 400:
                                            return this.handle400Error(req, next, error);
                                        case 401:
                                            return this.handle401Error(req, next);
                                    }
                                } else {
                                    return observableThrowError(error);
                                }
                            })
                        );
                })
            );
    }

    handle400Error(req, handle, error) {
        if (error && error.status === 400 && error.error && error.error.error === 'invalid_grant') {
            // If we get a 400 and the error message is 'invalid_grant', the token is no longer valid so logout.
            return this.logoutUser();
        }

        return observableThrowError(error);
    }

    handle401Error(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (!this.isRefreshingToken) {
            this.isRefreshingToken = true;

            const authService = this.injector.get(OAuthResourceService);

            return authService.getToken()
                .pipe(
                    switchMap((newToken: any) => {
                        console.log('newtoken ', newToken);
                        if (newToken) {
                            return next.handle(this.addToken(req, newToken.token));
                        }

                        // If we don't get a new token, we are in trouble so logout.
                        return this.logoutUser();
                    }),
                    catchError(error => {
                        // If there is an exception calling 'refreshToken', bad news so logout.
                        return this.logoutUser();
                    }),
                    finalize(() => {
                        this.isRefreshingToken = false;
                    })
                );
        } else {
            debugger; // we are trying to see if this ever gets hit, and maybe we shouldnt have it
            const authService = this.injector.get(OAuthResourceService);
            return authService.getToken()
                .pipe(
                    catchError(error => {
                        // If there is an exception calling 'refreshToken', bad news so logout.
                        return this.logoutUser();
                    }),
                    switchMap((newToken: any) => {
                        console.log('newtoken ', newToken);
                        if (newToken) {
                            return next.handle(this.addToken(req, newToken.token));
                        }
                    })
                );
        }
    }

    logoutUser() {
        this.oAuth.doLogout();
        return observableThrowError("Logging out user");
    }
}
