import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { OAuthResourceService } from './OAuthResource.service';


@Component({
    selector: 'loggedout',
    templateUrl: './loggedout.component.html'
})

export class LoggedOutComponent {

    public message: string = 'You might be logged in';

    constructor(public router: Router , private authService: OAuthResourceService) {
            //this.authService.doLogout();
            this.message = 'You are not logged in';
        
    }

    doLogin() {
    }

}