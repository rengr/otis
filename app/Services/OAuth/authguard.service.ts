import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, of as Observableof } from 'rxjs';
import { OAuthResourceService } from './OAuthResource.service';

@Injectable()
export class AuthGuardService implements CanActivate {
    constructor(private router: Router, private authService: OAuthResourceService) {
        //if (window.location.hostname === 'localhost') {
            this.authService.$isAuthenticatedObservable
                .subscribe(nextValue => {
                    if (!nextValue) {
                        this.router.navigate(['signin']);
                    }
                });
        //}

    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.authService.$isAuthenticatedObservable;
        // if (window.location.hostname === 'localhost')
        //     return this.authService.$isAuthenticatedObservable;
        // else
        //     return Observableof(true);
    }
}