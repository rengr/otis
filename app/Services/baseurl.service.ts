﻿import {Injectable} from '@angular/core';

declare var SERVICE_BASEURL: string;

@Injectable()
export class BaseUrlService {

     public baseUrl: string = SERVICE_BASEURL;
     //public baseUrl: string = 'http://localhost:57556/';

    public composeUrlWithBase(...urlParts: string[]): string {
        let i: number = 0;
        let result: string = this.baseUrl;
        for (; i < urlParts.length; ++i) {
            if (i > 0) {
                result += '/';
            }
            result += urlParts[i];
        }
        return result;
    }
}