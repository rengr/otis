import { EventEmitter, Injectable, ComponentFactoryResolver, Injector, ViewContainerRef, ReflectiveInjector, Output } from "@angular/core";
import { PopOverComponent } from  '../Chrome/popover.component';
import { FilterState } from  '../Models/filterStateModel';
import "rxjs/add/observable/from";
import "rxjs/add/operator/toArray";



@Injectable()
export class PopOverService {

  @Output() filtersEventEmmit: EventEmitter<FilterState> = new EventEmitter();

  constructor(
    private _resolver: ComponentFactoryResolver,
    private _injector: Injector) { }
    

  open(viewContainerRef: ViewContainerRef, name: string, filterState: FilterState) {
       viewContainerRef.clear();

    const popOverFactory = this._resolver.resolveComponentFactory(PopOverComponent)
    const injector = ReflectiveInjector.resolveAndCreate([], this._injector);

    const popOver = viewContainerRef.createComponent(popOverFactory, 0, injector);

    popOver.instance.sortOrderEventEmmit.subscribe((filters: FilterState) => this.announceSortOrder(filters));
    popOver.instance.setFilterState(filterState, name);

    popOver.instance.closeEventEmmit.subscribe((filters: FilterState) => { 
      this.filtersEventEmmit.emit(filters);
      popOver.destroy();
    });
  }

  announceSortOrder(filters: FilterState) {
    this.filtersEventEmmit.emit(filters);
  }

  areFilterStatesEqual(name: string, f1: FilterState, f2: FilterState): boolean {
    // same number of properties?
    if(Object.getOwnPropertyNames(f1).length != Object.getOwnPropertyNames(f2).length) { 
      return false;
    }
    
    // you are going to have to do more with these
    if(f1[name].columnName != f2[name].columnName){ return false;}
    if(f1[name].AtoZ != f2[name].AtoZ){ return false;}
    if(f1[name].ZtoA != f2[name].ZtoA){ return false;}
    if(f1[name].filterList.length != f2[name].filterList.length){ return false;}
    var list1 = f1[name].filterList;
    var list2 = f2[name].filterList;
    for(var i = 0; i < list1.length; ++i) {
      if(list1.isChecked != list2.isChecked) {
        return false;
      }
    }

    return false; //true;
  }

}