
import {timer as observableTimer, BehaviorSubject ,  Subject ,  Observable ,  Subscription } from 'rxjs';
import { tap, distinctUntilChanged } from 'rxjs/operators';
import { Notification, NotificationType } from '../Models/notificationModel';
import { Injectable } from '@angular/core';








@Injectable()
export class NotificationService {
    private notificationSubject: Subject<Notification>;
    private impendingClearSubscription: Subscription;
    notification$: Observable<Notification>;

    constructor() {
        this.notificationSubject = new Subject();
        this.notification$ = this.notificationSubject
        .pipe(
            distinctUntilChanged((lhs,rhs) => {
                var result = lhs == rhs;
                // we only care about comparing details is both sides are non-null
                if( !result && (lhs != null && rhs != null)) {
                result = (lhs.message == rhs.message && lhs.type == rhs.type);
                }
                return result;
            }),
            tap(nt => {
            if(nt == null) return;
            if(this.impendingClearSubscription) {
                this.impendingClearSubscription.unsubscribe();
                this.impendingClearSubscription = null;
            }
            this.impendingClearSubscription = 
                observableTimer(5000)
                .subscribe(unused => {
                    this.impendingClearSubscription = null;
                    this.notificationSubject.next(null);
                });
            })
        );
    }
    
    public sendNotification(msg: string, type: NotificationType){
        if(msg.length < 1){
            return;
        }
        this.notificationSubject.next({message: msg, type: type});
    }
    

}