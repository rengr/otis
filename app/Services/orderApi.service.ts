
import { of as observableOf, from as observableFrom,  Observable ,  BehaviorSubject ,  Subject ,  timer, combineLatest, Subscription } from 'rxjs';
import { filter, tap, distinctUntilChanged, catchError, debounce, map, switchMap, toArray, takeLast} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { HttpClient, HttpResponse } from "@angular/common/http";


import * as moment from 'moment';
import { BaseUrlService } from "./baseurl.service";
import { NotificationType } from '../Models/notificationModel';
import { Order, OrderListItem, OrderListItemBadges, OrderListItemShipping, OrderListItemSupport, OrderStatusDescriptions } from '../Models/orderModel';
import { OrderStatusEnum, ShipMethodEnum, OrderSupportTypeEnum } from '../Models/orderModel';
import { BadgeOrderStatusEnum, BadgeTypeEnum, BadgingServiceEnum} from '../Models/orderModel';
import { OrderItem, IdListItemConverter } from '../Models/orderItemModel';

import { LogService } from './log.service';
import { UtilityService } from './utilities';

const apiBaseUrl: string = "api/orders";
const queryUrl: string = 'api/OrdersTrackerSearch';

@Injectable()
export class OrderApiService {

 private announcingChangedOrder = new Subject<Order>();
 public announcingChangedOrder$ = this.announcingChangedOrder.asObservable();
 public orderList$: BehaviorSubject<Order[]>;
 public initialListAmount: string = '80';  // how many to pull down from server on initial call
 public searchSubscription : Subscription;

  constructor(
    private http: HttpClient,
    private baseUrl: BaseUrlService,
    private log: LogService,
    private util: UtilityService
  ) {
    this.orderList$ = new BehaviorSubject([]);
  }

  private mapOrder(rawOrder: any): Order {
    if (!rawOrder) return null;
    const valueIfDefined = this.util.valueIfDefined;
    let result = new Order();
    // required
    result.orderId                             = valueIfDefined(rawOrder.OrderId,0, Number);
    result.description                         = valueIfDefined(rawOrder.Description, null, String);
    result.accountOwner                        = valueIfDefined(rawOrder.AccountOwner, null, String);
    result.status                              = <OrderStatusEnum>valueIfDefined(rawOrder.Status, null, Number);
    //var status                                 = <OrderStatusEnum>valueIfDefined(rawOrder.Status, null, Number);
    result.statusDescription                   = result.status ? OrderStatusDescriptions[result.status] : '';
    result.organizationId                      = valueIfDefined(rawOrder.OrganizationId, null, Number);
    result.beginDate                           = valueIfDefined(rawOrder.BeginDate, null, String);
    result.projectDueDate                      = valueIfDefined(rawOrder.ProjectDueDate, null, String);

    result.organizationShortName               = valueIfDefined(rawOrder.OrganizationShortName, null, String);
    result.quoteNumber                         = valueIfDefined(rawOrder.QuoteNumber, null, Number);
    result.accountManager                      = valueIfDefined(rawOrder.AccountManager, null, String);
    result.projectLead                         = valueIfDefined(rawOrder.ProjectLead, null, String);
    result.orderDate                           = valueIfDefined(rawOrder.OrderDate, null, String);
    result.endDate                             = valueIfDefined(rawOrder.EndDate, null, String);
    result.shipDate                            = valueIfDefined(rawOrder.ShipDate, null, String);
    result.arrivalDate                         = valueIfDefined(rawOrder.ArrivalDate, null, String);
    result.equipmentReturned                   = valueIfDefined(rawOrder.EquipmentReturned, null, Boolean);
    result.postShowDeliveryDate                = valueIfDefined(rawOrder.PostShowDeliveryDate, null, String);
    result.location                            = valueIfDefined(rawOrder.Location, null, String);
    result.badgeType                           = BadgeTypeEnum[valueIfDefined(rawOrder.BadgeType, null, Number)];
    result.badgingCompany                      = valueIfDefined(rawOrder.BadgingCompany, null, String);
    result.badgeOrderStatus                    = BadgeOrderStatusEnum[(valueIfDefined(rawOrder.BadgeOrderStatus, null, Number))]
    result.badgingService                      = BadgingServiceEnum[valueIfDefined(rawOrder.BadgingService, null, Number)];
    result.badgeKitCost                        = valueIfDefined(rawOrder.BadgeKitCost, null, String);
    result.badgeKitOrderDate                   = valueIfDefined(rawOrder.BadgeKitOrderDate, null, String);
    result.badgeKitReceived                    = valueIfDefined(rawOrder.BadgeKitReceived, false, Boolean);
    result.badgeFormatAddedBy                  = valueIfDefined(rawOrder.BadgeFormatAddedBy, null, String);
    result.badgeNotes                          = valueIfDefined(rawOrder.BadgeNotes, null, String);
    result.customerContactId                   = valueIfDefined(rawOrder.CustomerContactId, null, Number);
    result.customerContactName                 = valueIfDefined(rawOrder.CustomerContactName, null, String);
    result.jobNumber                           = valueIfDefined(rawOrder.JobNumber, null, String);
    result.website                             = valueIfDefined(rawOrder.Website, null, String);
    result.boothNumber                         = valueIfDefined(rawOrder.BoothNumber, null, String);
    result.venue                               = valueIfDefined(rawOrder.Venue, null, String);
    result.notes                               = valueIfDefined(rawOrder.Notes, null, String);
    result.hours                               = valueIfDefined(rawOrder.Hours, null, String);
    result.shipMethod                          = <ShipMethodEnum>valueIfDefined(rawOrder.ShipMethod, null, Number);
    result.shipResponsibility                  = valueIfDefined(rawOrder.ShipResponsibility, null, String);
    result.shippingAddress                     = valueIfDefined(rawOrder.ShippingAddress, null, String);
    result.trackingNumbers                     = valueIfDefined(rawOrder.TrackingNumbers, null, String);
    result.returnTrackingNumbers               = valueIfDefined(rawOrder.ReturnTrackingNumbers, null, String);
    result.shippingNotes                       = valueIfDefined(rawOrder.ShippingNotes, null, String);
    result.support                             = OrderSupportTypeEnum[valueIfDefined(rawOrder.Support, null, Number)];
    result.supportTechs                        = valueIfDefined(rawOrder.SupportTechs, null, ((item)=>item));
    result.supportBadgeRequired                = valueIfDefined(rawOrder.SupportBadgeRequired, false, Boolean);
    result.supportDateIn                       = valueIfDefined(rawOrder.SupportDateIn, null, String);
    result.supportDateOut                      = valueIfDefined(rawOrder.SupportDateOut, null, String);
    result.supportHotelClientProvided          = valueIfDefined(rawOrder.SupportHotelClientProvided, false, Boolean);
    result.supportHotelBooked                  = valueIfDefined(rawOrder.SupportHotelBooked, false, Boolean);
    result.supportFlightBooked                 = valueIfDefined(rawOrder.SupportFlightBooked, false, Boolean);
    result.warehouseDeadline                   = valueIfDefined(rawOrder.WarehouseDeadline, null, String);
    result.warehouseStartDate                  = valueIfDefined(rawOrder.WarehouseStartDate, null, String);
    result.warehouseTarget                     = valueIfDefined(rawOrder.WarehouseTarget, null, String);
    result.supportTravelDateIn                 = valueIfDefined(rawOrder.SupportTravelDateIn, null, String);
    result.supportTravelDateOut                = valueIfDefined(rawOrder.SupportTravelDateOut, null, String);
    result.supportHotelAddress                 = valueIfDefined(rawOrder.SupportHotelAddress, null, String);
    result.onSiteContactId                     = valueIfDefined(rawOrder.OnSiteContactId, null, Number);
    result.supportNotes                        = valueIfDefined(rawOrder.SupportNotes, null, String);
    result.billingAddress                      = valueIfDefined(rawOrder.BillingAddress, null, String);
    result.billingEmail                        = valueIfDefined(rawOrder.BillingEmail, null, String);
    result.billingPhone                        = valueIfDefined(rawOrder.BillingPhone, null, String);
    result.billingCell                         = valueIfDefined(rawOrder.BillingCell, null, String);
    result.billingNotes                        = valueIfDefined(rawOrder.BillingNotes, null, String);
    result.hasCompliance                       = valueIfDefined(rawOrder.HasCompliance, false, Boolean);
    result.complianceInfo                      = valueIfDefined(rawOrder.ComplianceInfo, null, String);

    // clean up the dates
    const util = this.util;
    result.beginDate            = util.dateOnlyValue(result.beginDate);
    result.projectDueDate       = util.dateOnlyValue(result.projectDueDate);
    result.orderDate            = util.dateOnlyValue(result.orderDate);
    result.endDate              = util.dateOnlyValue(result.endDate);
    result.shipDate             = util.dateOnlyValue(result.shipDate);
    result.arrivalDate          = util.dateOnlyValue(result.arrivalDate);
    result.postShowDeliveryDate = util.dateOnlyValue(result.postShowDeliveryDate);
    result.badgeKitOrderDate    = util.dateOnlyValue(result.badgeKitOrderDate);
    result.supportDateIn        = util.dateOnlyValue(result.supportDateIn);
    result.supportDateOut       = util.dateOnlyValue(result.supportDateOut);
    result.supportTravelDateIn  = util.dateOnlyValue(result.supportTravelDateIn);
    result.supportTravelDateOut = util.dateOnlyValue(result.supportTravelDateOut);
    result.warehouseDeadline    = util.dateOnlyValue(result.warehouseDeadline);
    result.warehouseStartDate   = util.dateOnlyValue(result.warehouseStartDate);
    result.warehouseTarget      = util.dateOnlyValue(result.warehouseTarget);

    return result;
  }

  private mapOrderItem(raw: any): OrderItem {
    if (!raw) return null;
    const util = this.util;
    let result = new OrderItem();
    result.itemId             = util.valueIfDefined(raw.ItemId, 0, Number);
    result.orderId            = util.valueIfDefined(raw.OrderId, 0, Number);
    result.serviceId          = util.valueIfDefined(raw.ServiceId, null, Number);
    result.notes              = util.valueIfDefined(raw.Notes, null, String);
    result.serviceDescription = util.valueIfDefined(raw.ServiceDescription, null, String);
    result.serviceOptionIds   = util.valueIfDefinedArray(raw.ServiceOptionIds, null, IdListItemConverter);
    result.serviceName        = util.valueIfDefined(raw.ServiceName, null, String);

    return result;
  }

 public addToOrderPipe(orderObs: any){
   this.orderList$.next(orderObs);
 }

 public getAllOrders(){
  let url = this.baseUrl.composeUrlWithBase(apiBaseUrl, 'list');
  this.fetchOrderList(url);
}

public getSomeOrders(amount: string){
  let url = this.baseUrl.composeUrlWithBase(apiBaseUrl, 'list', amount);
  this.fetchOrderList(url);
}

  public fetchOrderList(url: string)
  {
    // let url = this.baseUrl.composeUrlWithBase(apiBaseUrl, "list");
   // this.log.logMessage(`You know, just thinking about getting some orders. --> (url?!?) ${url}`, NotificationType.info);
    this.http.get<Order[]>(url)
    // .pipe(  // ***** Leave this in for reference as another solution
    //     map(orderList => orderList.map(order => this.mapOrder(order)))
    //   )
      .subscribe(
        orderList => { 
          let mappedOrderList = orderList.map(order => this.mapOrder(order));
          this.addToOrderPipe(mappedOrderList);
        },
        error => this.log.logMessage(`Could not fetch order list. Got status ${error.statusText}`, NotificationType.error)
      );
  }
  
  public fetchBadgesList() {
    let url = this.baseUrl.composeUrlWithBase(apiBaseUrl, "badgeslist");
    this.http.get<OrderListItemBadges[]>(url)
      .subscribe(
        orderList => { 
          let mappedOrderList = orderList.map(order => this.mapOrder(order));
          this.addToOrderPipe(mappedOrderList);
        },
        error => this.log.logMessage(`Could not fetch badges order list. Got status ${error.statusText}`, NotificationType.error)
      );
  }
  public fetchShippingList(start : moment.Moment, end : moment.Moment) {
    let url = this.baseUrl.composeUrlWithBase(apiBaseUrl, "shippinglist", "?" + 'start=' + start.format('MM-DD-YYYY') + '&' + 'end=' + end.format('MM-DD-YYYY'));
      this.http.get<OrderListItemShipping[]>(url)
      .subscribe(
        orderList => { 
          let mappedOrderList = orderList.map(order => this.mapOrder(order));
          this.addToOrderPipe(mappedOrderList);
        },
        error => this.log.logMessage(`Could not fetch shipping order list. Got status ${error.statusText}`, NotificationType.error)
      );
  }
  
  public fetchSupportList() {
    let url = this.baseUrl.composeUrlWithBase(apiBaseUrl, "supportlist");
    this.http.get<OrderListItemSupport[]>(url)
    .subscribe(
      orderList => { 
        let mappedOrderList = orderList.map(order => this.mapOrder(order));
        this.addToOrderPipe(mappedOrderList);
      },
        error => this.log.logMessage(`Could not fetch support order list. Got status ${error.statusText}`, NotificationType.error)
      );
  }
  // private fetchOrderListIfNeeded() {
  //   if(this.orderList$.observers.length > 0) {
  //     this.fetchOrderList();
  //   }
  // }

  public getOrder(orderId: any): Observable<Order> {
    orderId = Number(orderId);
    if (!orderId || orderId < 1) {
      this.log.logMessage('orderId is not valid', NotificationType.error);
      return observableOf<Order>(null);
    }
    let url = this.baseUrl.composeUrlWithBase(
      apiBaseUrl,
      orderId.toString()
    );
    return this.http.get(url)
      .pipe(
        map(order => this.mapOrder(order)),
        catchError((error, thing) => {
          this.log.logMessage('Failed to get order with error: ' + error._body, NotificationType.error);
          return observableOf<Order>(null);
        })
      )
  }

  public updateOrder(order: Order): Observable<Order> {
    if (!order || order.orderId < 1) {
      this.log.logMessage('order is not valid', NotificationType.error);
      return observableOf<Order>(null);
    }
    let url = this.baseUrl.composeUrlWithBase(
      apiBaseUrl,
      order.orderId.toString()
    );
    return this.http.post<Order>(url, order)
      .pipe(
        map(response => {
          this.announcingChangedOrder.next(response);
          return this.mapOrder(response);
        }),
        catchError(error => {
        this.log.logMessage('Failed to update order with error: ' + error._body, NotificationType.error);
        return observableOf<Order>(null);
        })
      )
  }

  public createOrder(order: Order): Observable<Order> {
    if (!order || order.orderId > 0) {
      this.log.logMessage('Order is not valid for creation.', NotificationType.error);
      return observableOf<Order>(null);
    }
    order.orderId = 0;
    let url = this.baseUrl.composeUrlWithBase(apiBaseUrl);
    return this.http.post<Order>(url,order)
      .pipe(
        map(response => {
          this.announcingChangedOrder.next(response);
          return this.mapOrder(response);
        }),
        catchError(error => {
          this.log.logMessage('Failed to create order with error: ' + error._body, NotificationType.error);
          return observableOf<Order>(null);
        })
      )
  }

  public hideOrder(orderId : number) : Observable<boolean> {
    orderId = Number(orderId);
    if (!orderId || orderId < 1) {
      this.log.logMessage('orderId is not valid', NotificationType.error);
      return;
    }

    let url = this.baseUrl.composeUrlWithBase(apiBaseUrl, orderId.toString());
    return this.http.delete(url)
      .pipe(
        map(response => {
          this.announcingChangedOrder.next(null);
          return true;
        }),
        catchError(error => {
            this.log.logMessage('Failed to hide order with error: ' + error._body, NotificationType.error);
            return observableOf<boolean>(false);
        })
      )
  }

  public getOrderItems(orderId: number): Observable<OrderItem[]> {
    orderId = Number(orderId);
    if (!orderId || orderId < 1) {
      this.log.logMessage('orderId is not valid', NotificationType.error);
      return observableOf<OrderItem[]>(null);
    }
    let url = this.baseUrl.composeUrlWithBase(apiBaseUrl,orderId.toString(),'item/list');
    return this.http.get(url)
      .pipe(
        map((orderItemList: OrderItem[]) => orderItemList.map(orderItem => this.mapOrderItem(orderItem))),
        // map(order => this.mapOrderItem(order)),
        // toArray(),
        catchError((error, wut) => {
          this.log.logMessage('Failed to get orderItems with error: ' + error._body, NotificationType.error);
           return observableOf<OrderItem[]>(null);
        })
      )
  }
  public createOrderItem(newItem: OrderItem) : Observable<OrderItem> {
    if (!newItem || newItem.itemId > 0 || !newItem.orderId || newItem.orderId < 1) {
      this.log.logMessage('OrderItem is not valid for creation.', NotificationType.error);
      return observableOf<OrderItem>(null);
    }
    newItem.itemId = 0;
    let url = this.baseUrl.composeUrlWithBase(apiBaseUrl, newItem.orderId.toString(), 'item');
    return this.http.post(url,newItem)
      .pipe(
        map(response => this.mapOrderItem(response)),
        catchError(error => {
          this.log.logMessage('Failed to create orderItem with error: ' + error._body, NotificationType.error);
          return observableOf<OrderItem>(null);
        })
      )
  }
  public updateOrderItem(updatedItem: OrderItem) : Observable<OrderItem> {
    if (!updatedItem || updatedItem.itemId < 1 || !updatedItem.orderId || updatedItem.orderId < 1) {
      this.log.logMessage('OrderItem is not valid for update.', NotificationType.error);
      return observableOf<OrderItem>(null);
    }
    let url = this.baseUrl.composeUrlWithBase(apiBaseUrl, updatedItem.orderId.toString(), 'item', updatedItem.itemId.toString());
    return this.http.post(url,updatedItem)
      .pipe(
        map(response => this.mapOrderItem(response)),
        catchError(error => {
          this.log.logMessage('Failed to update orderItem with error: ' + error._body, NotificationType.error);
          return observableOf<OrderItem>(null);
        })
      )
  }
  public deleteOrderItem(orderId: number, orderItemId: number) : Observable<boolean> {
    orderItemId = Number(orderItemId);
    if (!orderItemId || orderItemId < 1) {
      this.log.logMessage('orderId is not valid', NotificationType.error);
      return;
    }

    let url = this.baseUrl.composeUrlWithBase(apiBaseUrl, orderId.toString(), 'item', orderItemId.toString());
    return this.http.delete(url)
      .pipe(
        catchError(error => {
          this.log.logMessage('Failed to delete orderItem with error: ' + error._body, NotificationType.error);
          return observableOf(error);
        })
      )
  }
  
  public startSearchOrder(terms: Observable<string>){
           this.searchSubscription = this.searchOrders(terms).subscribe(
             orderList => this.addToOrderPipe(orderList),
             error => this.log.logMessage(`Could not fetch order list. Got status ${error.statusText}`, NotificationType.error)
           );
  }         

   searchOrders(terms: Observable<string>){
            return terms
            .pipe(
              debounce(()=> timer(500)),
              filter(word  => word.length > 2 || word.length === 0),
              //takeLast(1),
              map(stringy => stringy.replace('/','-')),
              //distinctUntilChanged(),
              tap((term)=>{ console.log(term);}),
              switchMap((term: string) => {
                if(term.length === 0){
                  this.getSomeOrders(this.initialListAmount);
                  return observableOf([]);
                }
                else{
                  return this.searchOrdersAPI(term)
                }
              })
            )
   }
   

   closeSearchSubscription(){
     this.searchSubscription.unsubscribe();
   }
    
    searchOrdersAPI(term){
      return this.http.get<Order[]>(this.baseUrl.composeUrlWithBase(queryUrl,'orders',term))
              .pipe(
                map(orderList => orderList.map( order => this.mapOrder(order))),
            );
    }   
    
    closingDetails(){

    }


}
