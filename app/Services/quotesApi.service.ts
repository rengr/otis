
import {of as observableOf, from as observableFrom, throwError as observableThrowError,  Observable ,  BehaviorSubject ,  timer ,  Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders} from '@angular/common/http';
import { tap, toArray, map, filter, debounce, switchMap, catchError } from 'rxjs/operators';
import { BaseUrlService } from './baseurl.service';
import { Quote, QuoteStatus } from '../Models/QuoteModels';
import { NotificationType } from '../Models/notificationModel';
import { LogService } from './log.service';
import { UtilityService } from './utilities';


const apiBaseUrl: string = 'api/quotes';
const queryUrl: string = 'api/OrdersTrackerSearch';

@Injectable()
export class QuoteApiService {
    quoteList$: BehaviorSubject<Quote[]>
    private announcingChangedQuote = new Subject<Quote>();
    announcingChangedQuote$ = this.announcingChangedQuote.asObservable();
    public initialQuoteAmount: string = '50';  // how many to pull down from server on initial call

    constructor(
        private http: HttpClient,
        private baseUrl: BaseUrlService,
        private log: LogService,
        private util: UtilityService
    ) {
        this.quoteList$ = new BehaviorSubject([]);
    }

    /**
     * 
     * @param value Value from model.
     * @param defaultValue Value to return if value is undefined or null
     * @param converter Ctor to sanitize value
     */
    private valueIfDefined<T>(value: any, defaultValue: T, converter: (value?: any) => T): T {
        if (value === 'undefined' || value === null)
            return defaultValue;
        return converter(value);
    }

    mapQuote(rawQuote: any): Quote {
        if (!rawQuote)
            return null;
        let result = new Quote();
        result.quoteNumber             = this.valueIfDefined(rawQuote.QuoteNumber,0, Number);
        result.projectTitle            = this.valueIfDefined(rawQuote.ProjectTitle, null, String);
        result.salesPerson             = this.valueIfDefined(rawQuote.SalesPerson, null, String);
        result.organizationId          = this.valueIfDefined(rawQuote.OrganizationId, null, Number);
        result.contactId               = this.valueIfDefined(rawQuote.ContactId, null, Number);
        result.companyName             = this.valueIfDefined(rawQuote.CompanyName, null, String);
        result.contactName             = this.valueIfDefined(rawQuote.ContactName, null, String);
        result.quotationDate           = this.valueIfDefined(rawQuote.QuotationDate, null, String);
        result.businessType            = this.valueIfDefined(rawQuote.BusinessType, null, String);
        result.status                  = <QuoteStatus>this.valueIfDefined(rawQuote.Status, null, Number);
        result.billable                = this.valueIfDefined(rawQuote.Billable, false, Boolean);
        result.organizationDisplayName = this.valueIfDefined(rawQuote.OrganizationDisplayName, 'Unknown', String);
        result.contactDisplayName      = this.valueIfDefined(rawQuote.ContactDisplayName, 'Unknown', String);
        
        result.quotationDate            = this.util.dateOnlyValue(result.quotationDate);
        return result;
    }

    public addToQuotePipe(orderObs: any){
        this.quoteList$.next(orderObs);
    }

    public getAllQuotes(){
        let url = this.baseUrl.composeUrlWithBase(apiBaseUrl, 'list');
        this.fetchQuoteList(url);
    }

    public getSomeQuotes(amount: string){
        let url = this.baseUrl.composeUrlWithBase(apiBaseUrl, 'list', amount);
        this.fetchQuoteList(url);
    }

    public fetchQuoteList(url: string) {
        this.http.get<Quote[]>(url)
            .subscribe(
                quoteList => { 
                    let mappedQuoteList = quoteList.map(quote => this.mapQuote(quote));
                    this.quoteList$.next(mappedQuoteList);
                },
                error => this.log.logMessage(`Could not fetch the Quote list. Got status ${error.statusText}`, NotificationType.error)
            );
    }
    // private fetchQuoteListIfNeeded() {
    //     if(this.quoteList$.observers.length > 0) {
    //         this.getSomeQuotes(this.initialQuoteAmount);
    //     }
    // }

    public getQuote(quoteNumber: any): Observable<Quote> {
        quoteNumber = +quoteNumber;
        if (!quoteNumber || quoteNumber < 1) {
            return observableThrowError('Quote number is not valid');
        }
        let url = this.baseUrl.composeUrlWithBase(apiBaseUrl, quoteNumber.toString());
        return this.http.get(url)
        .pipe(
            map(response => {
                return this.mapQuote(response);
            })
        );
    }

    public updateQuote(quote: Quote): Observable<Quote> {
        if (!quote || quote.quoteNumber < 1) {
            this.log.logMessage('Quote is not valide.', NotificationType.error);
            return observableOf(null);
        }
        let url = this.baseUrl.composeUrlWithBase(apiBaseUrl, quote.quoteNumber.toString());
        return this.http.post(url, quote)
            .pipe(
                tap((quote: Quote)=> this.announcingChangedQuote.next(quote)),
                map(response => {
                     this.log.logMessage('Quote has been saved.', NotificationType.info);
                     return this.mapQuote(response);
                }),
                catchError((error) => {
                    this.log.logMessage('Could not update Quote ${quote.quoteNumber}. Got status ${error}', NotificationType.error);
                   return observableOf<Quote>(null);
                 })
            );
    }
    
    public createQuote(quote: Quote): Observable<Quote> {
        if (!quote || quote.quoteNumber > 0) {
            this.log.logMessage('Could not create Quote.', NotificationType.error); 
            return observableOf<Quote>(null);
        };
        let url = this.baseUrl.composeUrlWithBase(apiBaseUrl);

        return this.http.post(url, quote)
        .pipe(
               tap((quote: Quote)=> this.announcingChangedQuote.next(quote)),
           map(response => { 
             this.log.logMessage('Quote has been saved.', NotificationType.error);
             return this.mapQuote(response);
           }),
           catchError((error) => { 
             this.log.logMessage(`Could not create Quote ${quote.quoteNumber}. Got status ${error.status}`, NotificationType.error)
               return observableOf<Quote>(null);
           })
        )
    }

    searchQuotes(terms: Observable<string>){
        return terms
            .pipe(
                debounce(()=> timer(500)),
                filter(word  => word.length > 2 || word.length === 0),
                map(stringy => stringy.replace('/','-')),
                //.distinctUntilChanged()
                tap((term)=>{console.log(term);}),
                switchMap(term =>   {
                if(term.length === 0) {
                    this.getSomeQuotes(this.initialQuoteAmount);
                    return observableOf([]);
                }
                else{
                    return this.searchQuotesAPI(term);
                }})
            )
                .subscribe(
                    quoteList => this.addToQuotePipe(quoteList),
                    error => this.log.logMessage(`Could not fetch quotes list. Got status ${error.statusText}`, NotificationType.error)
                );
    }
    searchQuotesAPI(term){
        return this.http.get<Quote[]>(this.baseUrl.composeUrlWithBase(queryUrl,'quotes', term))
                .pipe(
                    map(quoteList => quoteList.map(quote => this.mapQuote(quote))),
                  );            
    }
}

