import {Injectable} from '@angular/core';

import {NotificationType} from '../Models/notificationModel';
import {NotificationService} from './notification.service';


@Injectable()
export class LogService{
  /**
   *
   */
  constructor(private notifyService: NotificationService) {
    
  }

  public logMessage(msg: string, type: NotificationType) {
    console.log(msg);
    this.notifyService.sendNotification(msg, type)
  }
}