import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
// import { HttpService } from './http.service';
import { Subject, Observable, BehaviorSubject, timer, from, of as observableOf } from 'rxjs';
import { catchError, filter, tap, toArray, switchMap, map, debounce } from 'rxjs/operators';

import { BaseUrlService } from './baseurl.service';
import { Contact } from '../Models/contactModel';
import { NotificationType } from '../Models/notificationModel';
import { LogService } from './log.service';
import { UtilityService } from './utilities';
import { IdListItemConverter } from '../Models/orderItemModel';


const apiBaseUrl: string = 'api/contacts';
const apiSearchContacts: string = 'api/orderstrackersearch/contacts';

@Injectable()
export class ContactApiService {
    
    contactList$: BehaviorSubject<Contact[]>
    private announcingChangedContact = new Subject<Contact>();
    announcingChangedContact$ = this.announcingChangedContact.asObservable();

    constructor(
        private http: HttpClient,
        private baseUrl: BaseUrlService,
        private log: LogService,
        private util: UtilityService
    ) {
        this.contactList$ = new BehaviorSubject([]);
    }

    mapContact(rawContact: any): Contact {
        if (!rawContact)
            return null;
        const util = this.util;
        let result = new Contact();
        result.contactId           = util.valueIfDefined(rawContact.ContactId,0, Number);
        result.firstName           = util.valueIfDefined(rawContact.FirstName, null, String);
        result.lastName            = util.valueIfDefined(rawContact.LastName, null, String);
        result.company             = util.valueIfDefined(rawContact.Company, null, String);
        result.email               = util.valueIfDefined(rawContact.Email, null, String);
        result.jobTitle            = util.valueIfDefined(rawContact.JobTitle, null, String);
        result.partnerId           = util.valueIfDefined(rawContact.PartnerId, null, Number);
        result.mobilePhone         = util.valueIfDefined(rawContact.MobilePhone, null, String);
        result.workPhone           = util.valueIfDefined(rawContact.WorkPhone, null, String);
        result.notes               = util.valueIfDefined(rawContact.Notes, null, String);
        result.partnerName         = util.valueIfDefined(rawContact.PartnerName, null, String);
        result.linkedOrganizations = util.valueIfDefinedArray(rawContact.LinkedOrganizations, null, IdListItemConverter);

        return result;
    }

    public addToContactPipe(orderObs: any){
        this.contactList$.next(orderObs);
    }

    public fetchContactList() {
        let url = this.baseUrl.composeUrlWithBase(apiBaseUrl, 'list');
        this.http.get<Contact[]>(url).pipe(
            catchError((error, caught) => { 
                this.log.logMessage(`Could not update Contact List. Got status ${error._body}`, NotificationType.error);
                return observableOf<Contact>(null);
            })
        )
        .subscribe(
           (contactList: Contact[]) => { 
               let mappedContactList = contactList.map(contact => this.mapContact(contact));
               this.contactList$.next(mappedContactList); 
            }
        );
    }
    private fetchcontactListIfNeeded() {
        if(this.contactList$.observers.length > 0) {
            this.fetchContactList();
        }
    }

    public getContact(ContactNumber: any): Observable<Contact> {
        ContactNumber = +ContactNumber;
        if (!ContactNumber || ContactNumber < 1) {
            //return Observable.throw('Contact number is not valid');
        }
        let url = this.baseUrl.composeUrlWithBase(apiBaseUrl, ContactNumber.toString());
        return this.http.get<Contact>(url)
            .pipe(
                map(response => {
                    return this.mapContact(response);
                })
            );
    }

    public updateContact(contact: Contact) {
        if (!contact || contact.contactId < 1) {
            this.log.logMessage('Contact is not valid.', NotificationType.error);
            return observableOf<Contact>(null);
        }
        let url = this.baseUrl.composeUrlWithBase(apiBaseUrl, contact.contactId.toString());
        return this.http.post<Contact>(url, contact)
        .pipe( 
                    tap((contact: Contact) => { 
                            this.announcingChangedContact.next(contact);
                    }),
                    map(response => {
                            this.log.logMessage('Contact has been saved.', NotificationType.info);
                            return this.mapContact(response);
                      }),
                    catchError((error, caught) => {
                        this.log.logMessage(`Could not update Contact. Got status ${error._body}`, NotificationType.error);
                        return observableOf<Contact>(null);
                    })
                )
    }
    
    public createContact(contact: Contact): Observable<Contact> {
        if (!contact || contact.contactId > 0) {
            this.log.logMessage('Could not create Contact.', NotificationType.error); 
            return observableOf<Contact>(null);
        };
        let url = this.baseUrl.composeUrlWithBase(apiBaseUrl);

        return this.http.post(url, contact).pipe(
            tap((contact) => this.announcingChangedContact.next()),
            map(response => { 
             this.log.logMessage('Contact has been saved.', NotificationType.error);
             return this.mapContact(response);
           }),
           catchError((error) => { 
             this.log.logMessage(`Could not create Contact ${contact.contactId}. Got status ${error._body}`, NotificationType.error);
               return observableOf<Contact>(null);
        }),)
    }


    searchContacts (terms: Observable<string>){
        return terms
                .pipe(debounce(()=> timer(500))
                 , filter(word  => word.length > 2 || word.length === 0)
                //.distinctUntilChanged()
                 , tap((term)=>{console.log(term);})
                 , switchMap(term => { 
                    if(term.length === 0 ){
                        this.fetchContactList();
                        return observableOf([]);
                    }
                    else{
                        return this.searchContactsAPI(term);
                }}))
                .subscribe(
                    custList => this.addToContactPipe(custList),
                    error => this.log.logMessage(`Could not fetch contacts list. Got status ${error._body}`, NotificationType.error)
                );
    }
    searchContactsAPI(term){
        return this.http.get<Contact[]>(this.baseUrl.composeUrlWithBase(apiSearchContacts, term))
        .pipe(
            map(contactList => contactList.map(contact => this.mapContact(contact))),
          );
    }
}

