// import { Injectable } from '@angular/core';
// import { HttpClient, RequestOptions } from '@angular/common/http';
// import { Observable } from 'rxjs';
// import { catchError, first } from 'rxjs/operators';
// import { OAuthResourceService, AccessToken } from './OAuth/OAuthResource.service';

// @Injectable()
// class HttpService extends HttpClient {
// constructor(options: RequestOptions, private auth: OAuthResourceService) {
//         super(options);
//     }

//     request(url: any, options?: RequestOptionsArgs): Observable<Response> {
//         return this.auth.getToken()
//             .first()
//             .switchMap((access) => {
//                 // Attached the Bearer token to the request
//                 if (typeof url === 'string') { // from angular http api --> "can either be a url or a Request instance"
//                     if (!options) {
//                         options = { headers: new Headers() };
//                     }
//                     options.headers.set('Authorization', 'Bearer ' + access.token);
//                     options.headers.append('Accept', 'application/json');
//                 } else {
//                     if (url.url) {
//                         url.headers.append('Accept', 'application/json');
//                         if (!url.headers.get('Authorization')) {
//                             url.headers.set('Authorization', 'Bearer ' + access.token);
//                         }
//                     }
//                 }

//                 return super.request(url, options).pipe(catchError<Response,Response>((error) => {
//                     //if got authorization error - try to update access token
//                     if (error.status == 401) {
//                         return this.auth.refreshAccessToken() // Get me a new token
//                             .switchMap((newAccess) => {
//                                 //if got new access token - retry request
//                                 if (newAccess) {
//                                     if (url.url) {
//                                         url.headers.set('Authorization', 'Bearer ' + newAccess.token);
//                                     } else {
//                                         if (!options) {
//                                             options = { headers: new Headers() };
//                                         }
//                                         options.headers.set('Authorization', 'Bearer ' + newAccess.token);
//                                         options.headers.append('Accept', 'application/json');
//                                     }
//                                     return super.request(url, options);
//                                 }
//                                 else {
//                                     return Observable.throw(new Error('Can\'t refresh the token'));
//                                 }
//                             });
//                     }
//                     else {
//                         console.log('throwing error in caught request');
//                         return Observable.throw(error);
//                     }
//                 }));
//             });
//     }
// }
