import { Injectable } from "@angular/core";


@Injectable()
export class UtilityService {
  /**
   *
   * @param value Value from model.
   * @param defaultValue Value to return if value is undefined or null
   * @param converter Ctor to sanitize value
   */
  valueIfDefined<T>( value: any, defaultValue: T, converter: (value?: any) => T ): T {
    if (value == undefined || value == null) return defaultValue;
    return converter(value);
  }
  valueIfDefinedArray<T>( value: any, defaultArrayValue: T, elementConverter: (value?: any) => T): Array<T> {
    if (value == undefined || value == null) return [];
    for(let i = 0; i < value.length; ++i) {
      value[i] = this.valueIfDefined(value[i], defaultArrayValue, elementConverter);
    }
    return value;
  }
  noData(value: any): boolean {
    if(value == 'undefined'|| value == null) {
      return true;
    }
    else if(value == {} ){
      return true;
    }
    else if(Array.isArray(value) && !value.length){  // todo to do TODO: change this last part
      return true;
    }
    else
      return false;
  }
  private dateRegex = /([0-9]{4}-[0-9]{2}-[0-9]{2})T[0-9]{2}:[0-9]{2}:[0-9]{2}/;
  dateOnlyValue(value: string): string {
    if(!value) return value;

    var regexResult = this.dateRegex.exec(value);
    if( regexResult[1] ) {
      return regexResult[1];
    }
    else {
      return value;
    }
  }

  valueOrNull<T>(value: T) : T {
    return value ? value : null;
  }
  valueOrEmptyStr<T>(value: T) : T|string {
    return value ? value : '';
  }
  trimmedStrOrNull(value: string) : string {
    let trimmedValue = value === null ? null : value.trim();
    return trimmedValue ? trimmedValue : null;
    
  }

  dateFormatter(date: Date){
    var localDate = new Date(date.getTime() + date.getTimezoneOffset()*60*1000);
   // var localDateString = localDate.toLocaleDateString();
   var dd = (localDate .getDate() < 10 ? '0' : '') + localDate.getDate();
   var MM = ((localDate.getMonth() + 1) < 10 ? '0' : '') + (localDate.getMonth() + 1);
   var yyyy = localDate.getFullYear();
   return (MM + "-" + dd + "-" + yyyy);
  }
}

