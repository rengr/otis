//import { HttpService } from './http.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { Observable, Subject, BehaviorSubject, timer } from "rxjs";
import { filter, switchMap, debounce, tap, distinctUntilChanged } from "rxjs/operators";
import { BaseUrlService } from './baseurl.service';


@Injectable()
export class SearchService {
// baseUrl: string = 'https://api.cdnjs.com/libraries';;
queryUrl: string = 'api/OrdersTrackerSearch';

  constructor( private _baseurl : BaseUrlService, private http: HttpClient) {}
 
  searchCustContacts(terms: Observable<string>){
    return terms.pipe(
            debounce(()=> timer(500)),
            filter(word  => word.length > 1),
            distinctUntilChanged(),
            tap((term) => { console.log(term) }),
            switchMap(term => this.searchCustContactAPI(term))
          ); 
  }
  searchCustContactAPI(term){
    return this.http.get(this._baseurl.composeUrlWithBase(this.queryUrl,'contacts',term));
  }
}