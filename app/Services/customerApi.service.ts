import { Customer } from "../Models/customerModels";
import { Injectable } from "@angular/core";
import { BaseUrlService } from "./baseurl.service";
import { Project } from "../Models/projectModel";
import { NotificationType } from '../Models/notificationModel';
import { LogService } from './log.service';
import { UtilityService } from './utilities';
import { HttpClient, HttpResponse } from '@angular/common/http';
// import { HttpService } from './http.service';
import { Subject, Observable, BehaviorSubject, timer, from, of as observableOf, defer } from 'rxjs';
import { catchError, filter, tap, toArray, switchMap, map, timeoutWith, debounce } from 'rxjs/operators';

const apiBaseUrl: string = "api/organization";
const queryUrl: string = 'api/OrdersTrackerSearch';

@Injectable()
export class CustomerApiService {
  customerList$ :  BehaviorSubject<Customer[]>;
  private announcingChangedCustomer = new Subject<Customer>();
  announcingChangedCustomer$ = this.announcingChangedCustomer.asObservable();

  constructor(
    private http: HttpClient,
    private baseUrl: BaseUrlService,
    private log: LogService,
    private utils: UtilityService
  ) {
    this.customerList$ = new BehaviorSubject([]);
  }

  public createCustomer(cust: Customer): Observable<Customer> {
    if (!cust || Number(cust.organizationId) > 0) { 
      this.log.logMessage('Could not create Customer.', NotificationType.error); 
      return observableOf<Customer>(null);
    };
    let url = this.baseUrl.composeUrlWithBase(apiBaseUrl);

    return this.http.post<Customer>(url, cust).pipe(
       tap((cust)=> this.announcingChangedCustomer.next(cust)),
       map(response => { 
        this.log.logMessage('Customer has been saved.', NotificationType.error);
        //this.announcingChangedCustomer.next(response);
        return this.mapCustomer(response);
      }),
      catchError((error, thing) => { 
        this.log.logMessage(`Could not create Customer ${cust.organizationId}. Got status ${error.status}`, NotificationType.error)
          return observableOf<Customer>(null);
      }));
  
    }
  
  public getCustomer(organizationId: any): Observable<Customer> {
    if (!organizationId || Number(organizationId) < 1) { 
      this.log.logMessage('Could not retrieve Customer.', NotificationType.error); 
      return observableOf<Customer>(null);
    };
    let url = this.baseUrl.composeUrlWithBase(apiBaseUrl, organizationId.toString());

    return this.http.get<Customer>(url)
    .pipe(
        map(response => {
          return this.mapCustomer(response);
        }),
       catchError((error) => {
          this.log.logMessage('Could not fetch Customer ${organizationId}. Got status ${response.statusText}', NotificationType.error);
          return observableOf<Customer>(null);
       }),);
    }


  public updateCustomer(cust: Customer): Observable<Customer> {
     if (!cust || Number(cust.organizationId) < 1) { 
      this.log.logMessage('Could not retrieve Customer.', NotificationType.error); 
       return observableOf<Customer>(null);
     }
    let url = this.baseUrl.composeUrlWithBase( apiBaseUrl, cust.organizationId.toString());
    
    return this.http.post<Customer>(url, cust)
    .pipe(
      tap((cust)=> this.announcingChangedCustomer.next(cust)),
      map(response => {
          this.log.logMessage('Customer has been saved.', NotificationType.info);
         // this.announcingChangedCustomer.next(new Customer());
          return this.mapCustomer(response);
        }),
       catchError((error) => {
          this.log.logMessage(`Could not update Customer ${cust.organizationId}. Got status ${error._body}`, NotificationType.error);
         return observableOf<Customer>(null);
       })
      );
   }

  public hideCustomer(customerId : number) : Observable<boolean> {
    let url = this.baseUrl.composeUrlWithBase( apiBaseUrl, customerId.toString());

    return this.http.delete(url).pipe(
        map(resp => true), // if 200 then we want to return true
        tap(()  => this.fetchCustomerListIfNeeded()),
        catchError((error)=> {
          this.log.logMessage(`Could not delete Customer ${customerId}. Got status ${error._body}`, NotificationType.error)
          return observableOf<boolean>(false);
        }),);
   }
  
  mapCustomer(rawCustomer: any): Customer {
    if (!rawCustomer) return null;
    const util = this.utils;
    let result = new Customer();

    result.organizationId               = util.valueIfDefined(rawCustomer.OrganizationId,0, Number);
    result.name                         = util.valueIfDefined(rawCustomer.Name, null, String);
    result.shortName                    = util.valueIfDefined(rawCustomer.ShortName, null, String);
    result.partnerId                    = util.valueIfDefined(rawCustomer.PartnerId, null, Number);
    result.ftpUserName                  = util.valueIfDefined(rawCustomer.FtpUserName, null, String);
    result.ftpPassword                  = util.valueIfDefined(rawCustomer.FtpPassword, null, String);
    result.backendUserName              = util.valueIfDefined(rawCustomer.BackendUserName, null, String);
    result.backendPassword              = util.valueIfDefined(rawCustomer.BackendPassword, null, String);
    result.shippingAccount              = util.valueIfDefined(rawCustomer.ShippingAccount, null, String);
    result.notes                        = util.valueIfDefined(rawCustomer.Notes, '', String);
    result.templateNotes                = util.valueIfDefined(rawCustomer.TemplateNotes, '', String);

    return result;
  }

    public addToCustomerPipe(orderObs: any){
        this.customerList$.next(orderObs);
    }

  public fetchCustomerList(){
    let url = this.baseUrl.composeUrlWithBase(apiBaseUrl, "list");
    this.http.get<Customer[]>(url)
    .pipe(
      timeoutWith(30000, defer(()=>{ this.log.logMessage('Getting customer list timed out.', NotificationType.error); }))
    ) 
    .subscribe(
          customerList => {
            let mappedCustomerList = customerList.map(customer => this.mapCustomer(customer));
            this.customerList$.next(mappedCustomerList)
          },
          error =>  this.log.logMessage(`There was a problem getting the customer list ${error.statusText === undefined ? error.message : error.statusText}`, NotificationType.error)
    );
  }

  private fetchCustomerListIfNeeded() {
    if(this.customerList$.observers.length > 0) {
      this.fetchCustomerList();
    }
  }

  public getOrders(organizationId: any): Observable<Project[]> {
    if (!organizationId || Number(organizationId) < 1) {
      this.log.logMessage('customer number is not valid', NotificationType.error);
    }
    let url = this.baseUrl.composeUrlWithBase(apiBaseUrl, organizationId.toString(), 'projectlist');
    
    return this.http.get(url).pipe(
        map(response => this.utils.valueIfDefined<Project[]>(response,[],Array)),    
        catchError( error => {
            this.log.logMessage('Could not fetch orders List ${organizationId}. Got status ${error}', NotificationType.error);
            return observableOf<Project[]>(null);        }),);
  }
         
  public getHandler(organizationId: any): Observable<string[]> {
    if (!organizationId || Number(organizationId) < 1) {
      this.log.logMessage('customer number is not valid', NotificationType.error);
      return observableOf(null);
    }
    let url = this.baseUrl.composeUrlWithBase( apiBaseUrl, organizationId.toString(), 'handlers');

    return this.http.get(url).pipe(
        map(response => this.utils.valueIfDefined<string[]>(response,[], Array)),
        catchError( error => {
            this.log.logMessage('Could not get exhibit house.', NotificationType.error);
            return observableOf<string[]>(null);
        }),);
    }

    searchCustomers (terms: Observable<string>){
      return terms
          .pipe(
              debounce(()=> timer(500)),
              filter((word: string)  => word.length > 2 || word.length === 0),
              map(stringy => stringy.replace('/','-')),
              //.distinctUntilChanged()
              tap((term)=>{console.log(term);}),
              switchMap(term => { 
                if(term.length === 0 ){
                  this.fetchCustomerList();
                  return observableOf([]);
                }
                else{
                  return this.searchCustomersAPI(term);
                }
              })
              )
              .subscribe(
                custList => this.addToCustomerPipe(custList),
                error => this.log.logMessage(`Could not fetch customers list. Got status ${error.statusText}`, NotificationType.error)
              );
    }
    searchCustomersAPI(term){
      return this.http.get<Customer[]>(this.baseUrl.composeUrlWithBase(queryUrl,'customers',term))
        .pipe(
          map(custList => custList.map(cust => this.mapCustomer(cust))),
        );
    }

}
