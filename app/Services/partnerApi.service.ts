
import {from as observableFrom,  Observable ,  BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { toArray, map, switchMap, catchError } from 'rxjs/operators';

import { BaseUrlService } from "./baseurl.service";
import { NotificationType } from '../Models/notificationModel';
import { Partner } from '../Models/partnerModel';

import { LogService } from './log.service';
import { UtilityService } from './utilities';

const apiBaseUrl: string = "api/partners";

@Injectable()
export class PartnerApiService {
  public partnerList$: BehaviorSubject<Partner[]>;

  constructor(
    private http: HttpClient,
    private baseUrl: BaseUrlService,
    private log: LogService,
    private util: UtilityService
  ) {
    this.partnerList$ = new BehaviorSubject([]);
    this.fetchPartnerList();
  }

  private mapPartner(rawPartner: any): Partner {
    if (!rawPartner) return null;
    const valueIfDefined = this.util.valueIfDefined;
    let result = new Partner();
    // required
    result.partnerId = valueIfDefined(rawPartner.PartnerId,null, Number);
    result.name      = valueIfDefined(rawPartner.Name, null, String);

    return result;
  }

  public fetchPartnerList() {
   let url = this.baseUrl.composeUrlWithBase(apiBaseUrl, "list");
    this.http.get<Partner[]>(url)
    .pipe(
      //.map(response => response.json())
      //switchMap(partnerArray => observableFrom(partnerArray)),
      map(rawPartner => rawPartner.map( singlePartner => this.mapPartner(singlePartner))),
      // toArray()
    )
      .subscribe(
        partnerList => this.partnerList$.next(partnerList),
        // orderList => this.orderList$.next(orderList),
        error => this.log.logMessage(`Could not fetch partner list. Got status ${error.statusText}`, NotificationType.error)
      );
  }
 
  public createPartner(name : string){
    console.log('creating partner');
    let url = this.baseUrl.composeUrlWithBase(apiBaseUrl);
     this.http.post(url, { Name : name})
      //.map(response => response.json())
      .pipe( 
        map(rawPartner => this.mapPartner(rawPartner))
      )
      .subscribe(
        partner => {
          this.log.logMessage('Created partner: ' + partner.name, NotificationType.info);
          this.fetchPartnerList();
        },
        error => this.log.logMessage(`Could not create partner. Got status ${error.statusText}`, NotificationType.error)
      );

  }

  public editPartner(partner : Partner){
    console.log('edit partner');
    let url = this.baseUrl.composeUrlWithBase(apiBaseUrl, partner.partnerId.toString());
     this.http.post(url, { Name : partner.name, PartnerId : partner.partnerId})
      //.map(response => response.json())
      .pipe(
        map(rawPartner => this.mapPartner(rawPartner))
      )
      .subscribe(
        partner => { 
          this.log.logMessage('Updated partner: ' + partner.name, NotificationType.info)
          this.fetchPartnerList();
        },
        error => this.log.logMessage(`Could not update partner. Got status ${error.statusText}`, NotificationType.error)
      );


  }

  public deletePartner(partner : Partner){
    console.log('deleting partner');
    partner.partnerId = Number(partner.partnerId);
    if (!partner || partner.partnerId < 1) {
      this.log.logMessage('partner is not valid', NotificationType.error);
      return;
    }

    let url = this.baseUrl.composeUrlWithBase(apiBaseUrl, partner.partnerId.toString());
    this.http.delete(url)
    .pipe(
      map(response => {
        this.log.logMessage('Deleted partner: ' + partner.name, NotificationType.info)
        return true;
      })
    )
    .subscribe(
        data => {
          this.log.logMessage('Deleted partner: ' + partner.name, NotificationType.info)
          this.fetchPartnerList();
      },
      error => this.log.logMessage(`Could not delete partner. Got status ${error.statusText}`, NotificationType.error)
     );
  }

  public getPartnerRelationships(name : string){
    console.log('gettting relationships');
  }

  private fetchPartnerListIfNeeded() {
    if(this.partnerList$.observers.length > 0) {
      this.fetchPartnerList();
    }
  }
}
