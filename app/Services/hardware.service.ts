
import {of as observableOf, from as observableFrom,  Observable ,  Subject ,  BehaviorSubject } from 'rxjs';
import { filter, tap, distinctUntilChanged, catchError, debounce, map, switchMap, toArray} from 'rxjs/operators';
import { OrderItem } from '../Models/orderItemModel';
import { Injectable } from "@angular/core";
import { HttpClient, HttpResponse } from '@angular/common/http';
// import { HttpService } from './http.service';

import { BaseUrlService } from "./baseurl.service";
import { NotificationType } from '../Models/notificationModel';

import { LogService } from './log.service';
import { UtilityService } from './utilities';
import { HardwareRentalItem, HardwareListItem } from '../Models/hardwareModel';

const hardwareListUri: string = 'api/inventory/products';
const hardwareBaseUri: string = 'api/orders';

@Injectable()
export class HardwareService {
  private _hardwareListItems = new BehaviorSubject<HardwareListItem[]>([]);
  private _orderHardwareList$: BehaviorSubject<HardwareRentalItem[]>;
  
  private _addedToOrder$: BehaviorSubject<{}>;

  constructor(
    private http: HttpClient,
    private baseUrl: BaseUrlService,
    private log: LogService,
    private util: UtilityService
  ) {
    this.fetchHardwareList();
    this._orderHardwareList$ = new BehaviorSubject([]);
    this._addedToOrder$ = new BehaviorSubject([]);
  }
  
  get hardwareList$() {
    return this._hardwareListItems.asObservable();
  }

  get orderHardwareList$(){
    return this._orderHardwareList$.asObservable();
  } 
  addedToOrder(){
    return this._addedToOrder$.asObservable();
  } 

  private fetchHardwareList() {
    const url = this.baseUrl.composeUrlWithBase(hardwareListUri, 'list/enabled');
    this.http.get<HardwareListItem[]>(url)
    .pipe(
      //switchMap(hardwareArray  => observableFrom(hardwareArray)),
      map(hwList => hwList.map(hardwareItem => this.mapHardwareListItem(hardwareItem))),
      // toArray()
    )
    .subscribe(hwList => this._hardwareListItems.next(hwList));
  }

  getHardwareForService(orderItemId: number): Observable<HardwareRentalItem[]> {
    const url = this.baseUrl.composeUrlWithBase(hardwareBaseUri, 'items', orderItemId.toString(), 'rentals');
    return this.http.get<HardwareRentalItem[]>(url)
    .pipe(
        //switchMap(rentalArray  => observableFrom(rentalArray)),
       map(hwRentalList => hwRentalList.map(rentalItem => this.mapHardwareRentalItem(rentalItem))),
        // toArray()
    );
  } 

  createHardwareRental(rentalItem: HardwareRentalItem): Observable<HardwareRentalItem> {
    if(!rentalItem || rentalItem.itemId > 0) {
      return null;
    }
    const url = this.baseUrl.composeUrlWithBase(hardwareBaseUri, 'rentals');
    return this.http.post<HardwareRentalItem>(url, rentalItem)
    .pipe(
        map(res => this.mapHardwareRentalItem(res)),
        // tap((newRentalItem: HardwareRentalItem) => { 
        //   this.getHardwareForOrder(newRentalItem.orderId); 
        // }),
        catchError((error) => {
          this.log.logMessage('Could not add hardware to order. (' + error.error + ')', NotificationType.error);
          return observableOf(null);
        })
     );
  }
  updateHardwareRental(rentalItem: HardwareRentalItem): Observable<HttpResponse<any>> {
    if(!rentalItem || rentalItem.itemId < 1) {
      return null;
    }
    const url = this.baseUrl.composeUrlWithBase(hardwareBaseUri, 'rentals');
    return this.http.put<HttpResponse<any>>(url, rentalItem);
  }
  deleteHardwareRental(rentalItemId: number, orderId: number) : Observable<HttpResponse<any>> {
    if(!rentalItemId || rentalItemId < 1) {
      return null;
    }
    const url = this.baseUrl.composeUrlWithBase(hardwareBaseUri, 'rentals', rentalItemId.toString());
    return this.http.delete<HttpResponse<any>>(url);
  }

  getHardwareForOrder(orderId: number) {
    if(!orderId || orderId < 1) { return; }
    const url = this.baseUrl.composeUrlWithBase(hardwareBaseUri, orderId.toString(), 'rentals');
    // const url = this.baseUrl.composeUrlWithBase(hardwareBaseUri, 'items', orderId.toString(), 'rentals');
    return this.http.get<HttpResponse<any>>(url)
    .pipe(
        map((rentalList: HardwareRentalItem[]) => rentalList.map(rentalItem => this.mapHardwareRentalItem(rentalItem))),
        // toArray()
    )
    .subscribe(
      (rentalArray: HardwareRentalItem[])  => {
          this._orderHardwareList$.next(rentalArray);
        }, 
       error => {
         this.log.logMessage('broke on getting order rentals list', NotificationType.error)
     });
  }

  private mapHardwareRentalItem(rawItem: any) : HardwareRentalItem {
    const util = this.util;
    var result = new HardwareRentalItem();
    /* Rquired */
    result.itemId             = util.valueIfDefined(rawItem.ItemId, 0, Number);
    result.rentalProductId    = util.valueIfDefined(rawItem.RentalProductId, 0, Number);
    result.quantity           = util.valueIfDefined(rawItem.Quantity, 0, Number);
    result.backupDevices      = util.valueIfDefined(rawItem.BackupDevices, 0, Number);
    result.orderId            = util.valueIfDefined(rawItem.OrderId, 0, Number);
    /* Rquired */
    result.orderItemId        = util.valueIfDefined(rawItem.OrderItemId, null, Number);
    result.notes              = util.valueIfDefined(rawItem.Notes, null, String);
    result.productDescription = util.valueIfDefined(rawItem.Description, null, String);
    return result;
  }
  private mapHardwareListItem(rawItem: any) : HardwareListItem {
    const util = this.util;
    var result = new HardwareListItem();
    result.productId   = util.valueIfDefined(rawItem.ProductId, 0, Number);
    result.description = util.valueIfDefined(rawItem.Description, null, String);
    return result;
    
  }
}