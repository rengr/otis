
import {throwError as observableThrowError, BehaviorSubject,  Observable } from 'rxjs';

import {map} from 'rxjs/operators';

import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';





import { BaseUrlService } from './baseurl.service';
import { Router } from '@angular/router';
import { User } from '../Models/userprofile';
import { Employee, EmployeeType } from '../Models/employeeModel';

@Injectable()
export class EmployeeService {
    private _employees$: BehaviorSubject<Employee[]>;
    private userProfile: { user: User[] };

    constructor(private _http: Http, private _baseurl : BaseUrlService, private router : Router) {
        
        // this is hardcoded until we get the real api working
        this._employees$ = new BehaviorSubject([
            {userName: 'alexakin',        displayName: 'Alex Schmitt',          type: [EmployeeType.operations, EmployeeType.support] },
            {userName: 'alextong',        displayName: 'Alex Tong',             type: [EmployeeType.operations, EmployeeType.sales, EmployeeType.support] },
            {userName: 'cmarshall49',     displayName: 'Chris Marshall',        type: [EmployeeType.operations, EmployeeType.sales, EmployeeType.support] },
            {userName: 'ClaudineMcKague', displayName: 'Claudine McKague',      type: [EmployeeType.operations] },
            {userName: 'ethanwood',       displayName: 'Ethan Wood',            type: [EmployeeType.developer] },
            {userName: 'griffengorsuch',  displayName: 'Griffen Gorsuch',       type: [EmployeeType.developer] },
            {userName: 'jonjackson',      displayName: 'Jon Jackson',           type: [EmployeeType.operations, EmployeeType.sales, EmployeeType.support] },
            {userName: 'katherine',       displayName: 'Katherine Nelson',      type: [EmployeeType.operations] },
            {userName: 'michelle',        displayName: 'Michelle Bradford',     type: [EmployeeType.operations] },
            {userName: 'scott',           displayName: 'Scott Wilson',          type: [EmployeeType.developer] },
            {userName: 'Shaynametzner',   displayName: 'Shayna Metzner',        type: [EmployeeType.operations, EmployeeType.sales] },
        ]);
    }

    get userEmployees$() {
        return this._employees$.asObservable();
    }

    // this is just a stub for when we get real data from an api
    public getEmployees() {
        let headers = new Headers();
        headers.append('Accept', '*/*');
        return this._http.get(this._baseurl.baseUrl + 'api/mebbeEmplayeeskis?!!!??****', { headers: headers }).pipe(
            map((response: Response) => <Employee[]>response.json()))
            .subscribe(data => {
                // console.log(data);
                // maybe manipulate here, depending on what we get back
                this._employees$.next(data);
            }, error => observableThrowError(error || 'Server error'));
    }
}