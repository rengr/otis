
import {from as observableFrom,  Observable ,  Subject ,  BehaviorSubject } from 'rxjs';
import { Injectable } from "@angular/core";
import { HttpClient, HttpResponse, HttpHeaders} from '@angular/common/http';
import { toArray, map, switchMap, catchError } from 'rxjs/operators';

import { BaseUrlService } from "./baseurl.service";
import { NotificationType } from '../Models/notificationModel';
import { Service, ServiceOption } from '../Models/serviceModels';

import { LogService } from './log.service';
import { UtilityService } from './utilities';

const apiBaseUrl: string = "api/service";

@Injectable()
export class ServiceApiService {
  private serviceListSubject: BehaviorSubject<Service[]>;
  private serviceOptionListSubject: BehaviorSubject<ServiceOption[]>;
  serviceList$: Observable<Service[]>;
  serviceOptionList$: Observable<ServiceOption[]>;


  constructor(
    private http: HttpClient,
    private baseUrl: BaseUrlService,
    private log: LogService,
    private util: UtilityService
  ) {
    this.serviceListSubject = new BehaviorSubject(null);
    this.serviceList$ = this.serviceListSubject.asObservable();

    this.serviceOptionListSubject = new BehaviorSubject([]);
    this.serviceOptionList$ = this.serviceOptionListSubject.asObservable();

    this.fetchServiceList();
  }

  private mapService(raw: any): Service {
    if (!raw) return null;
    const util = this.util;
    let result = new Service();
    result.serviceId   = util.valueIfDefined(raw.ServiceId, 0, Number);
    result.description = util.valueIfDefined(raw.Description, null, String);
    
    return result;
  }
  private mapServiceOption(raw: any): ServiceOption {
    if (!raw) return null;
    const util = this.util;
    let result = new ServiceOption();
    result.optionId     = util.valueIfDefined(raw.OptionId, 0, Number);
    result.serviceId    = util.valueIfDefined(raw.ServiceId, 0, Number);
    result.description  = util.valueIfDefined(raw.Description, null, String);
    
    return result;
  }

  fetchServiceList() {
    var url = this.baseUrl.composeUrlWithBase(apiBaseUrl, 'list')
    this.http.get<Service[]>(url)
      //.map(response => response.json())
      .pipe(
       //switchMap(serviceArray => observableFrom(serviceArray)),
       map(service => service.map(srv => this.mapService(srv)).sort((a, b) => a.description.localeCompare(b.description))),
      //  toArray()
      )
      .subscribe(nextServiceArr => {
        if(nextServiceArr != null ) {
          this.serviceListSubject.next(nextServiceArr);
          this.fetchOptionList();
        }
      },
      error => this.log.logMessage(`Could not fetch service list. Got status ${error.statusText}`, NotificationType.error));
  }
  fetchOptionList() {
    var url = this.baseUrl.composeUrlWithBase(apiBaseUrl, 'optionlist')
    this.http.get<ServiceOption[]>(url)
      //.map(response => response.json())
      .pipe(
       //switchMap(optionArray => observableFrom(optionArray)),
       map(service => service.map(srv => this.mapServiceOption(srv))),
      //  map(srv => this.mapServiceOption(srv)),
      // toArray()
      )
      .subscribe(nextOptionArr => {
        if(nextOptionArr != null ) {
          this.serviceOptionListSubject.next(nextOptionArr);
        }
      },
      error => this.log.logMessage(`Could not fetch service option list. Got status ${error.statusText}`, NotificationType.error));
  }
}
