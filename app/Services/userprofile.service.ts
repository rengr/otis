
import {throwError as observableThrowError,  Observable, BehaviorSubject } from 'rxjs';

import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { BaseUrlService } from './baseurl.service';
import { Router } from '@angular/router';
import { User } from '../Models/userprofile';

@Injectable()
export class UserProfileService {
    //private UrlBase: string;
    private customHeaders: Headers;
    private _userProfile$: BehaviorSubject<User[]>;
    private userProfile: { user: User[] };


    constructor(private _http: HttpClient, private _baseurl : BaseUrlService, private router : Router) {


        this.customHeaders = new Headers();
        this.customHeaders.append('Accept', 'application/json');

        this.userProfile = { user: [] };
        this._userProfile$ = <BehaviorSubject<User[]>>new BehaviorSubject([]);
        //this.currentEvent = { event: [] };
        //this._currentEvent$ = <BehaviorSubject<IEvent[]>>new BehaviorSubject([]);

        //this.UrlBase = _baseurl.baseUrl;
    }

    get userProfile$() {
        return this._userProfile$.asObservable();
    }

    //get currentevent$() {
    //    return this._currentEvent$.asObservable();
    //}

    public logout() {
        const httpOptions = { 
            headers : new HttpHeaders({'Accept':'*/*'})
        }
        return this._http.post(this._baseurl.baseUrl  + 'logout', '', httpOptions)
            .toPromise()
            .catch(this.handleError);
    }

    public getUserProfile() {
        const httpOptions = { 
            headers : new HttpHeaders({'Accept':'*/*'})
        }
        return this._http.get<User[]>(this._baseurl.baseUrl + 'api/account', httpOptions)
            // .pipe(
            //     map((response: Response) => response.json())
            // )
            .subscribe(
                (data) => {
                    this.userProfile.user = data;
                    this._userProfile$.next(this.userProfile.user);
                }, 
                error => observableThrowError(error || 'Server error'));
    }

    //public login() {
    //    let headers = new Headers();
    //    headers.append('Accept', '*/*');
    //    return this._http.get(this.UrlBase + 'login?ReturnUrl=%2fOrders')
    //        .toPromise()
    //        .catch(this.handleError);
    //}

    private handleError(error: Response) {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        console.error(error);
        return observableThrowError(error || 'Server error');
    }
}