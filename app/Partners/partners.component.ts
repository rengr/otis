import { CommonModule } from '@angular/common';
import { Component, OnDestroy, OnInit} from '@angular/core';
import { Observable ,  BehaviorSubject ,  Subject } from 'rxjs';
import { Router } from '@angular/router';

import { PartnerApiService } from '../Services/partnerApi.service';
import { Partner } from '../Models/partnerModel';
// import { EmployeeService } from '../Services/employees.service';


@Component({
    templateUrl: './partners.component.html',
    styleUrls:['./partners.component.scss'],
    selector: 'partners'
})
export class PartnersComponent implements OnInit {

    partners$ : Observable<Partner[]>;
    showDeleteConfirm : boolean = false;
    showEdit : boolean = false;
    newPartner : string = '';
    editPartner : Partner;
    HidePartners : boolean = false;

    constructor(
        private partnerService: PartnerApiService) {    }

    ngOnInit(): void {
        this.partners$ = this.partnerService.partnerList$;
        this.partnerService.fetchPartnerList();
    }

    // onKey(event : any){
    //     this.newPartner += event.target.value;
    // }

    showhide(){
        this.HidePartners = !this.HidePartners;
        this.showEdit = this.showDeleteConfirm = false;
    }
    
    MakeChanges(partner : Partner){
        this.showEdit = true;
        this.showDeleteConfirm = false;
        this.editPartner = partner;
    }

    showCreate(){
        this.showEdit = this.showDeleteConfirm = false;
        this.editPartner = null;
    }

    Create(newPartnerName : string) {
        if(newPartnerName.length > 0){
            this.partnerService.createPartner(newPartnerName);
            this.newPartner = '';
        }        
    }

    Edit_Save(newName : string){
        this.showEdit = this.showDeleteConfirm = false;
        if(newName.length < 1 ) { return; }

        this.editPartner.name = newName;
        this.partnerService.editPartner(this.editPartner);
    }

    Delete_Ask() {
        this.showDeleteConfirm = true;
        this.showEdit = false;
    }
    
    Delete_Okay(){
        this.showDeleteConfirm = false;
        this.partnerService.deletePartner(this.editPartner); 
    }

    showRelatives(partner: Partner){
        this.partnerService.getPartnerRelationships('baz');
    }
}



