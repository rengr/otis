import { Component, OnDestroy, OnInit, HostListener } from '@angular/core';
import { NotificationService } from '../Services/notification.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap} from '@angular/router';
import { Observable ,  BehaviorSubject } from 'rxjs';


import { QuoteApiService } from '../Services/quotesApi.service';
import { Quote, QuoteStatus } from '../Models/QuoteModels';

import { CustomerApiService } from '../Services/customerApi.service';
import { Customer } from '../Models/customerModels'
import { Employee } from '../Models/employeeModel';
import { EmployeeService } from '../Services/employees.service';
import { getLocaleDateTimeFormat } from '@angular/common';
import { DialogService } from '../Services/dialog.service';

@Component({
    selector: 'quote-details',
    templateUrl: './quoteDetails.component.html',
})
export class QuoteDetailsComponent implements OnDestroy, OnInit {
    title: string;
    quoteNumber: number;
    form: FormGroup;
    organizationList$: BehaviorSubject<Customer[]>;
    employees$: Observable<Employee[]>;


    constructor(
        private quoteService: QuoteApiService,
        private notification: NotificationService,
        private orgService: CustomerApiService,
        private employeeService: EmployeeService,
        private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder,
        private dialogService: DialogService
    ) {
        this.organizationList$ = new BehaviorSubject<Customer[]>(null);
        this.employees$ = employeeService.userEmployees$;
        this.createForm();
    }



    ngOnInit(): void {
        this.route.paramMap.subscribe((params) => {
            this.quoteNumber = +(params.get('quoteNumber'));
            if( this.quoteNumber > 0 ) { // new quote form.
                this.title = "Quote";
                this.quoteService.getQuote(this.quoteNumber)
                    .subscribe(result => {
                        this.updateForm(result);
                    });
            }
            else {
                this.title = "New Quote";
                this.form.reset();
                this.form.patchValue({quotationDate : new Date().toISOString().split('T')[0]}); //Date.Today.toString("mm/dd/yyy")});
            }
            this.orgService.customerList$.subscribe(orgList => {
                orgList.sort((a,b)=> {
                    return a.shortName.toLocaleLowerCase() < b.shortName.toLocaleLowerCase() ? -1 : 1;
                });
                this.organizationList$.next(orgList);
            })
            this.orgService.fetchCustomerList()
        });
    }

    private createForm() {
        this.form = this.formBuilder.group({
            projectTitle: ['',Validators.required],
            salesPerson: ['',Validators.required],
            status: [null,Validators.required],
            companyName: '',
            contactName: '',
            quotationDate: '2018-03-22', //Date.now(),
            businessType: '',
            billable: false,
            organizationId: null,
            // contactId: null,
        });
    }
    private updateForm(quote: Quote) {
        let form = this.form;

        form.setValue({
            projectTitle: quote.projectTitle,
            salesPerson: quote.salesPerson,
            status: quote.status,
            companyName: quote.companyName,
            contactName: quote.contactName,
            quotationDate: quote.quotationDate,
            businessType: quote.businessType,
            billable: quote.billable,
            organizationId: quote.organizationId
        });
     form.markAsPristine();
    }

    onSubmit() {
        let quote = this.prepareSaveQuote();
        if(quote === null ) { return ; }

        let request:Observable<Quote> = null;
        if( quote.quoteNumber == 0 ) {
            request = this.quoteService.createQuote(quote);
        }
        else { request = this.quoteService.updateQuote(quote); }
        
        request
        .subscribe((updatedQuote: Quote):void => {
            if(updatedQuote.quoteNumber != this.quoteNumber) {
                this.router.navigate(['../../quote-details',updatedQuote.quoteNumber], {relativeTo:this.route});
            }
            this.updateForm(updatedQuote);
        });
    }


    // onDelete(){
    //     this.quoteService.deleteQuote(this.form.value.quoteNumber)
    //         .subscribe(
    //             data => { 
    //                 this.notification.sendNotification('item has been deleted', null);
    //                 this.router.navigate(['/customers']);
    //             },
    //             error => console.log('send this error to the error service', error),
    //             () => console.log('completed deleted')
    //         );
    // }

    private prepareSaveQuote(): Quote {
        if( !this.form.valid ) {
            alert("Form isn't valid!");
            return;
        }
        const formModel = this.form.value;
        const saveQuote: Quote = new Quote();
        /* Required */
        saveQuote.quoteNumber = this.quoteNumber;
        saveQuote.projectTitle = formModel.projectTitle;
        saveQuote.salesPerson = formModel.salesPerson;
        saveQuote.status = formModel.status;
        /* Required */
        saveQuote.companyName = formModel.companyName;
        saveQuote.contactName = formModel.contactName;
        saveQuote.quotationDate = formModel.quotationDate;
        saveQuote.businessType = formModel.businessType;
        saveQuote.organizationId = formModel.organizationId;
        saveQuote.billable = formModel.billable == null ? false : formModel.billable;

        return saveQuote;
    }

    
 canDeactivate(): Observable<boolean> | boolean {
    if (!this.form.pristine) {
        return this.dialogService.confirm('YOU WILL LOSE YOUR CHANGES IF YOU CONTINUE');
    }
    return true;
  }	

 @HostListener('window:beforeunload', ['$event'])
 public beforeunloadHandler($event) {
     if(!this.form.pristine){
        $event.returnValue = "Discard changes for this Order?";
     }
}


    ngOnDestroy() {

    }
}