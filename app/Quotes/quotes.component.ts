
import {combineLatest, tap} from 'rxjs/operators';
import { SearchService } from '../Services/search.service';
import { CommonModule } from '@angular/common';
import { Component, OnDestroy, OnInit} from '@angular/core';
//import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Observable ,  BehaviorSubject ,  Subject } from 'rxjs';
import { Router } from '@angular/router';

import { QuoteApiService } from '../Services/quotesApi.service';
import { Quote } from '../Models/QuoteModels';
import { EmployeeService } from '../Services/employees.service';


@Component({
    templateUrl: './quotes.component.html',
    styleUrls: ['./quotes.component.scss'],
})
export class QuotesComponent implements OnDestroy, OnInit {

    headers$: Observable<string[]>;
    columns$: Observable<string[]>;
    quotes$: Observable<Quote[]>; 
    showDetailsBoolean: boolean = false;
    searchTerm$ = new Subject<string>();
    showShowAll: boolean = true;

    constructor(
        private quoteService: QuoteApiService,
        private employeeService: EmployeeService,
        private router: Router,
        private search: SearchService
    ) {
        this.headers$ = new BehaviorSubject([
            'Quote Number',
            'Status',
            'Project Title',
            'Company',
            'Quotation Date',
            'Sales Person',
            'Business Type'
        ]);
        this.columns$ = new BehaviorSubject([
            'quoteNumber',
            'statusDisplay',
            'projectTitle',
            'companyName',
            'quotationDate',
            'salesPerson',
            'businessType'
        ]);
    }

    ngOnInit(): void {
        this.quotes$ = this.quoteService.quoteList$;
        this.quoteService.getSomeQuotes(this.quoteService.initialQuoteAmount);
        this.quotes$.pipe(
        combineLatest(this.employeeService.userEmployees$, (quotes,employees) => {
            quotes.forEach((q) => {
                let sp = employees.find((emp) => emp.userName == q.salesPerson);
                if(sp) { q.salesPerson = sp.displayName; }
            });
            return quotes;
        }))
        .subscribe(
            ord=> { 
                ord.sort((a,b) => { 
                if(a.quoteNumber < b.quoteNumber) {  return  1;};
                if(a.quoteNumber > b.quoteNumber) {  return -1;};
                return 0;})
            },
            error => console.log(error),
            () =>  console.log('quotes subscription is finished') 
        );
        
        this.searchTerm$
            .subscribe(
                term => this.showShowAll = term.length < 1 ? true : false
            );

        this.quoteService.searchQuotes(this.searchTerm$);

        this.quoteService.announcingChangedQuote$.subscribe(quote => {
                var searchTerm = <HTMLInputElement>document.getElementById('searchBox');
                this.searchTerm$.next(searchTerm.value);
        });
    }

    showAll(){
        this.showShowAll = false;
        this.quoteService.getAllQuotes();
    }

    ngOnDestroy() {

    }

    onButtonClick(quote: Quote) {
        this.router.navigate(['/quote-details', quote.quoteNumber]);
    }
    
    onActivate(event: any) {
        this.showDetailsBoolean = true;
    }
    onDeactivate(event: any) {
        this.showDetailsBoolean = false;
    }

}