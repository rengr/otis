
import {from as observableFrom,  Observable ,  BehaviorSubject } from 'rxjs';

import {combineLatest, map, flatMap} from 'rxjs/operators';
import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { OrderApiService } from '../Services/orderApi.service';
import { ServiceApiService } from '../Services/serviceApi.service';
import { OrderItem, IdListItem } from '../Models/orderItemModel';
import { Service, ServiceOption } from '../Models/serviceModels';

import { UtilityService } from '../Services/utilities';

@Component({
  selector: 'order-items',
  styleUrls: ['./orderItems.component.scss'],
  templateUrl: './orderItems.component.html',
})
export class OrderItemsComponent implements OnDestroy, OnInit {
  private _orderId: number = null;
  get orderId(): number { return this._orderId; }
  @Input()
  set orderId(orderId: number) {
    this._orderId = orderId;
    // "reset" the current orderItem to nothing.
    this.currentOrderItemId = null;
    this.showDetails = false;
    // get the list of orderItems
    this.fetchItemList(orderId);
  }
  private serviceOptions$: Observable<ServiceOption[]>;
  //private orderItemsSubject: BehaviorSubject<OrderItem[]>;
  orderItems$: BehaviorSubject<OrderItem[]>;
  services$: Observable<Service[]>;
  selectedServiceOptions$: BehaviorSubject<ServiceOption[]>;

  noServiceOptions: boolean = false;
  noSelectedOptions: boolean = false;
  showDetails: boolean = false;
  hideUnselectedOptions: boolean = false;
  showDelete: boolean = false;
  currentOrderItemId: number = null;
  private currentItemServiceOptions: number[] = [];
  private pristineServiceOptions: IdListItem[] = [];
  public form: FormGroup;


  constructor(
    private orderService: OrderApiService,
    private servicesService: ServiceApiService,
    private router: Router,
    private formBuilder: FormBuilder,
    private utils : UtilityService
  ) {
    this.services$ = servicesService.serviceList$;
    this.serviceOptions$ = servicesService.serviceOptionList$;

    //this.orderItemsSubject = new BehaviorSubject([]);
    this.orderItems$ = new BehaviorSubject([]);
    this.selectedServiceOptions$ = new BehaviorSubject([]);

    this.createForm();

  }

  ngOnInit(): void {
    this.services$ = this.servicesService.serviceList$;

    this.form.get('serviceId').valueChanges.pipe(
      combineLatest(this.serviceOptions$, (newServiceId, serviceOptionArr) => {
        if (newServiceId == null) {
          return [];
        }
        return serviceOptionArr.filter(srvOpt => srvOpt.serviceId == newServiceId);
      }))
      .subscribe(serviceOptionList => {
        this.noServiceOptions = serviceOptionList.length == 0;

        for (var i = this.currentItemServiceOptions.length - 1; i > -1; --i) {
          const me = this.currentItemServiceOptions[i];
          if (serviceOptionList.find(serviceOption => serviceOption.optionId == me) == null) {
            this.currentItemServiceOptions.splice(i, 1); // remove me
          }
        }
        
        serviceOptionList.forEach(serviceOpt => {
          serviceOpt.uiIsSelected = this.currentItemServiceOptions.indexOf(serviceOpt.optionId) > -1;
        });
        this.selectedServiceOptions$.next(serviceOptionList)
      });
  }
  ngOnDestroy() {
    this.services$ = null;
  }
  doToggleRowExpand(orderItem: OrderItem) {
    if (this.form.dirty && !confirm("Are you sure you want to leave and lose your changes?"))
      return;
    this.updateForm(orderItem);
  }
  doCreate() {
    if (this.form.dirty && !confirm("Are you sure you want to leave and lose your changes?"))
      return;
    this.updateForm(new OrderItem());
    this.currentOrderItemId = 0;
    this.showDetails = true;
  }

  private fetchItemList(orderId: number) {
    if (orderId < 1)
      return;
    this.orderService.getOrderItems(orderId)
    .pipe(
        combineLatest(this.services$, this.serviceOptions$, (itemArray, servicesArr: Service[], serviceOptionsArr: ServiceOption[]) => {
          if (itemArray == null) return null;
          if(servicesArr == null) return null;
          itemArray.map(item => { 
            //item.updateServiceDescription(servicesArr);
            item.updateOptionsDisplay(serviceOptionsArr);
          });  
          return itemArray;
        })
        
      )
      .subscribe(newList => {
        if (newList == null)
          return;
        this.orderItems$.next(newList);
      });
      }

  onSubmit() {
    if (!this.form.valid) {
      return;
    }
    let orderItem = this.prepareSaveOrderItem();
    if (orderItem.itemId > 0) {
      this.orderService.updateOrderItem(orderItem)
        .subscribe((updatedItem: OrderItem) => {
          if (updatedItem == null) {
            return;
          }
          this.fetchItemList(this.orderId);
          this.updateForm(updatedItem);
        });
    } else {
      this.orderService.createOrderItem(orderItem)
        .subscribe((newItem: OrderItem) => {
          if (newItem == null) {
            return;
          }
          this.fetchItemList(this.orderId);
          this.updateForm(newItem);
        });
    }
  }
  onDeleteCurrentItem() {
    if (confirm("Are you sure you want to delete this item?")) {
      this.orderService.deleteOrderItem(this.orderId, this.currentOrderItemId)
        .subscribe(didDelete => {
          this.showDetails = false;
          this.currentOrderItemId = null;
          this.fetchItemList(this.orderId);
        });
    }
  }

  private createForm() {
    this.pristineServiceOptions = [];
    this.form = this.formBuilder.group({
      itemId: [null],
      serviceId: [null],
      notes: [''],
      serviceDescription: '',
      options: this.formBuilder.array([])
    });
  }

  // this should really be handled by the form, but this was quicker than doing forms in forms and binding
  checkBoxChecked(option: ServiceOption){
    if(option.uiIsSelected){
      option.uiIsSelected = false;
      this.currentItemServiceOptions.splice(this.currentItemServiceOptions.indexOf(option.optionId),1);
    } else {
      option.uiIsSelected = true;
      this.currentItemServiceOptions.push(option.optionId);
    }
    option.uiIsSelected = !option.uiIsSelected;
    this.form.markAsDirty();
  }

  private updateForm(item: OrderItem) {
    let form = this.form;

    this.currentOrderItemId = item.itemId;
    
    this.showDetails = true;
    this.showDelete = item.itemId > 0;
    this.hideUnselectedOptions = item.serviceOptionIds.length > 0;
    this.pristineServiceOptions = item.serviceOptionIds;
    this.currentItemServiceOptions = [];
    item.serviceOptionIds.forEach(optId => this.currentItemServiceOptions.push(optId.id))


    form.patchValue({
      itemId: item.itemId,
      // required
      serviceId: this.utils.valueOrEmptyStr(item.serviceId),
      notes: item.notes,
      serviceDescription: item.serviceDescription,
      serviceName:  item.serviceName
    });
    form.markAsPristine();
  }
  private prepareSaveOrderItem(): OrderItem {
    const formModel = this.form.value;
    const saveOrderItem: OrderItem = new OrderItem();
    /* Required */
    saveOrderItem.itemId = this.currentOrderItemId;
    saveOrderItem.orderId = this.orderId;
    /* Required */
    saveOrderItem.serviceId = formModel.serviceId;
    saveOrderItem.notes = formModel.notes;
    saveOrderItem.serviceDescription = formModel.serviceDescription;
    // prepare the option list
    this.currentItemServiceOptions.forEach(optId => saveOrderItem.serviceOptionIds.push({id: optId, delete: false, name:null}));

    this.pristineServiceOptions.forEach(prisOpt => {
      if(this.currentItemServiceOptions.indexOf(prisOpt.id) == -1) {
        prisOpt.delete = true;
        saveOrderItem.serviceOptionIds.push(prisOpt);
      }
    });

    // Collect and store the selected options
    return saveOrderItem;
  }
}
