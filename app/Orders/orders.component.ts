﻿
import {from as observableFrom, of as ObservableOf, Observable,  Subject ,  BehaviorSubject } from 'rxjs';

import {map, combineLatest, withLatestFrom,  pairwise, take, distinctUntilChanged } from 'rxjs/operators';
import { SearchService } from '../Services/search.service';
import { Order, OrderStatusEnum, OrderListItem, GeneralListHeaders, badgeListHeaders, shippingListHeaders, supportListHeaders } from '../Models/orderModel';
import { Component, OnDestroy, OnInit, ViewContainerRef, ViewChild, ViewChildren } from '@angular/core';


import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap} from '@angular/router';
import * as moment from 'moment';
import { OrderApiService } from '../Services/orderApi.service';
import { NotificationType } from '../Models/notificationModel';
import { EmployeeService } from '../Services/employees.service';
import { UtilityService } from '../Services/utilities';
import { Employee } from '../Models/employeeModel';
import * as _ from 'lodash';
import { PopOverService } from  '../Services/popover.service';
import { CanComponentDeactivate } from '../Services/dirtyFormGuard.service';


@Component({
    templateUrl: './orders.component.html',
    styleUrls: ['./orders.component.scss'],
})
export class OrdersComponent implements OnDestroy, OnInit {
    
    headersSubject: BehaviorSubject<{}[]>;
    headers$: Observable<{}[]>;
    orders$: Observable<Order[]>; 
    showDetailsBoolean: boolean = false;
    searchTerm$ = new Subject<string>();
    hasSearch$ = new BehaviorSubject(false);
    columnLength: number = 30; // this is our description length, could be done in css maybe?
    filterList: {};
    _currentTab: string = 'master';
    showShowAll: boolean = true;

    @ViewChildren('popoverTemplate', {read:ViewContainerRef}) container: Array<ViewContainerRef>;

    constructor(
        private orderService: OrderApiService,
        private router: Router,
        private search: SearchService,
        private exployeeService: EmployeeService,
        private utils: UtilityService,
        private popoverservice: PopOverService
    ) {
        this.headersSubject = new BehaviorSubject(GeneralListHeaders);
        this.headers$ = this.headersSubject.pipe(distinctUntilChanged());
    }

    openPopOver(event: any){
        // var target = event.target || event.srcElement || event.currentTarget;
        // var idAttribute = target.id;
        // for(var i = 0; i < this.container.length; ++i){
        // // var yac = document.getElementById(idAttribute);
        // if(this.container._results[i]._data.renderElement.id === idAttribute){
        //     var foo = this.container.toArray()[i];
        // this.popoverservice.open(foo, idAttribute);
        // break;
        // }
        // }
    }

    get currentTab() : string {
        return this._currentTab;
    }
    set currentTab(newTab:string) {
        if(!newTab) throw "currentTab cannot be falsy";

        if(newTab != this._currentTab) {
            this._currentTab = newTab;
            switch(newTab) {
                default:
                case 'master':
                    this.getMasterList();
                    break;
                case 'badges':
                    this.getBadgesList();
                    break;
                case 'shipping':
                    this.getShippingList();
                    break;
                case 'support':
                    this.getSupportList();
                    break;
            }
        }
    }


    comparer = (a,b) => {
            if(a.beginDate < b.beginDate) return 1;
            if(a.beginDate > b.beginDate) return -1;
            return 0;
    };

    ngOnInit(): void {

        // group by tests, ERASE THIS
        var myarr = [ 
            { Phase: "Phase 1", Step: "Step 1", Task: "Task 1", Value: "5" },
            { Phase: "Phase 1", Step: "Step 1", Task: "Task 2", Value: "10" },
            { Phase: "Phase 1", Step: "Step 2", Task: "Task 1", Value: "15" },
            { Phase: "Phase 1", Step: "Step 2", Task: "Task 2", Value: "20" },
            { Phase: "Phase 2", Step: "Step 1", Task: "Task 1", Value: "25" },
            { Phase: "Phase 2", Step: "Step 1", Task: "Task 2", Value: "30" },
            { Phase: "Phase 2", Step: "Step 2", Task: "Task 1", Value: "35" },
            { Phase: "Phase 2", Step: "Step 2", Task: "Task 2", Value: "40" }
        ]
        var a = _.groupBy(myarr, function(n) {
            return n.Phase;
          });
        console.log('ayyyy ', a);
        // end ERASE THIS  

        let mapEmployee = (user: string, employees: Employee[]) => {
            let empResult = employees.find(emp => emp.userName == user);
            return empResult ? empResult.displayName : user;
        };

        this.orders$ = this.orderService.orderList$;
        
        this.orders$
        .pipe(
                map((orders)=> orders.sort(this.comparer)),
                combineLatest(this.exployeeService.userEmployees$, (orderArray, employees) => {
                    observableFrom(orderArray)
                    .subscribe((order)=>{
                        if(order.description == null) return;// change length of Description, maybe this should be done in css
                        order.description = order.description.substring(0,this.columnLength);
                        order.accountManager = mapEmployee(order.accountManager, employees);
                        order.accountOwner = mapEmployee(order.accountOwner, employees);
                        order.beginDate = order.beginDate ? this.utils.dateFormatter(new Date(order.beginDate)) : '';
                        order.endDate = order.endDate ? this.utils.dateFormatter(new Date(order.endDate)) : '';
                        order.shipDate = order.shipDate ? this.utils.dateFormatter(new Date(order.shipDate)) : '';
                        order.arrivalDate = order.arrivalDate ? this.utils.dateFormatter(new Date(order.arrivalDate)) : '';
                        order.badgeKitOrderDate = order.badgeKitOrderDate ? this.utils.dateFormatter(new Date(order.badgeKitOrderDate)) : '';
                        order.projectDueDate = order.projectDueDate  ? this.utils.dateFormatter(new Date(order.projectDueDate)) : '';
                    })
                    //console.log('county ', orderArray.length);
                    return orderArray;
                })
            )
            .subscribe(data => {
                var foo = data.map(o => o);
                this.filterList = data;
            }); 

        this.getMasterList(); 
        
        this.searchTerm$
            .subscribe(
                term => this.showShowAll = term.length < 1 ? true : false
            );

        this.orderService.startSearchOrder(this.searchTerm$); //, this.hasSearch$);
        
        this.orderService.announcingChangedOrder$
        .pipe(
            withLatestFrom(this.hasSearch$, this.headersSubject, this.searchTerm$, (alteredOrder, hasSearch, headers, term) => {
                if((<HTMLInputElement>document.getElementById('searchBox')).value != ''){
                    this.orderService.searchOrders(ObservableOf(<string>term));
                    return;
                }
                if(headers == GeneralListHeaders){ 
                    this.orderService.getAllOrders();
                }
                else if(headers == badgeListHeaders) {
                    this.orderService.fetchBadgesList();
                }
                else if(headers == supportListHeaders) {
                    this.orderService.fetchSupportList();
                }
                else if(headers == shippingListHeaders) {
                    this.orderService.fetchShippingList(moment(), moment().add(4, 'w'));
                }
            })
        )
        .subscribe();
    }


    changeOrder(event: any){
        var target = event.target || event.srcElement || event.currentTarget;
        var idAttribute = target.id;
        var newSortDirection;

        // find out which direction we need to go
        this.headers$.subscribe((x) => {
            observableFrom(x)
                .subscribe((x: any)  => { 
                    x.idName === idAttribute ? newSortDirection = x.sortDirection : null;
                });  
            });

        // do sorting
        var reggy = RegExp('[0-9]{2}-[0-9]{2}-[0-9]{4}'); // this could obviously be more robust
        this.orders$.subscribe((ord)=> {
            ord.sort((a,b) => {
                
                if( a[idAttribute] === null || a[idAttribute] === undefined || new Date(a[idAttribute]).getTime() === NaN ) { return newSortDirection; };
                if( b[idAttribute] === null || b[idAttribute] === undefined || new Date(b[idAttribute]).getTime() === NaN ) { return newSortDirection * -1;};
                
                if(!reggy.test(a[idAttribute]) || !reggy.test(b[idAttribute]) ){
                    // console.log('regex just told me this is not a date ');
                    if( a[idAttribute].replace(/\s+/g,'').toLowerCase() <    b[idAttribute].replace(/\s+/g,'').toLowerCase() ) {  return newSortDirection;};
                    if( a[idAttribute].replace(/\s+/g,'').toLowerCase() >    b[idAttribute].replace(/\s+/g,'').toLowerCase() ) {  return newSortDirection * -1;};
                    if( a[idAttribute].replace(/\s+/g,'').toLowerCase()  === b[idAttribute].replace(/\s+/g,'').toLowerCase())  {  return  0;};
                } else {
                    // console.log('regex just told me this IS a date ');
                    return newSortDirection > 0 ? new Date(a[idAttribute]).getTime() - new Date(b[idAttribute]).getTime() : new Date(b[idAttribute]).getTime() - new Date(a[idAttribute]).getTime();
                }
            });
        });

        // change $headers sortDirection
        this.headers$
        .subscribe(x => {  
            observableFrom(x)
            .subscribe((y:any)=>{
                y.idName === idAttribute ? y.sortDirection *= -1 : null;
            }) 
        });
    }
                        
    private getMasterList():void{
        this.headersSubject.next(GeneralListHeaders);
        this.orderService.getSomeOrders(this.orderService.initialListAmount);
        (<HTMLInputElement>document.getElementById('searchBox')).value = '';
    }

    private getShippingList():void{
        this.headersSubject.next(shippingListHeaders);
        this.orderService.fetchShippingList(moment().subtract(1, 'w'), moment().add(4, 'w'));
        (<HTMLInputElement>document.getElementById('searchBox')).value = '';
    }

    private getSupportList():void{
        this.headersSubject.next(supportListHeaders);
        this.orderService.fetchSupportList();
        // this.orders$ = this.orders$.map(orders => orders.filter(o => o.supportTech 
        //                                                             || o.supportDateIn
        //                                                             || o.supportDateOut 
        //                                                             || o.supportTravelDateIn 
        //                                                             || o.supportTravelDateOut
        //                                                             || o.supportNotes 
        //                                                         ));
        (<HTMLInputElement>document.getElementById('searchBox')).value = '';
    }

    private getBadgesList():void {
        this.headersSubject.next(badgeListHeaders);
        this.orderService.fetchBadgesList();
        (<HTMLInputElement>document.getElementById('searchBox')).value = '';
    }

    showAll(){
        this.showShowAll = false;
        this.orderService.getAllOrders();
    }

    closingDetails(){
        this.orderService.closingDetails();
    }

    ngOnDestroy() {
        this.orderService.closeSearchSubscription();
    }
    oldTarget: any;
    onClick(event: any){
        if(this.oldTarget){
            this.oldTarget.style.backgroundColor = '';
            this.oldTarget.style.fontWeight = '';
            this.oldTarget.style.color = '';
        }
        event.currentTarget.style.backgroundColor = '#0072cf';
        event.currentTarget.style.fontWeight = 'bold';
        event.currentTarget.style.color = '#ffffff';
        this.oldTarget = event.currentTarget;

        this.showDetailsBoolean = true;
    }
    onButtonClick(order: Order) {
        this.router.navigate(['/order-details', order.orderId]);
    }
    onActivate(event: any) {
        this.showDetailsBoolean = true;
    }
    onDeactivate(event: any) {
        this.showDetailsBoolean = false;
    }
    // public canDeactivate() : boolean {
    //     debugger;
    //     // Allow synchronous navigation (`true`) if no crisis or the crisis is unchanged
    //     if (true){ //(!this.crisis || this.crisis.name === this.editName) {
    //       return true;
    //     }
    //     // Otherwise ask the user with the dialog service and return its
    //     // observable which resolves to true or false when the user decides
    //     //return this.dialogService.confirm('Discard changes?');
    //     return true;
    //   }
}