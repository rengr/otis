
import {map, combineLatest, tap} from 'rxjs/operators';

import { Component, OnDestroy, OnInit, HostListener, ViewChild } from '@angular/core';
import { HardwareService } from '../Services/hardware.service';
import { HardwareListItem, HardwareRentalItem } from '../Models/hardwareModel';
import { SearchService } from '../Services/search.service';
import { Observable ,  BehaviorSubject ,  Subject } from 'rxjs';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap, NavigationEnd} from '@angular/router';
import { OrderApiService } from '../Services/orderApi.service';
import { CustomerApiService } from '../Services/customerApi.service';
import { NotificationService } from '../Services/notification.service';
import { Order, OrderStatusEnum } from '../Models/orderModel';
import { ContactApiService } from '../Services/contactsApi.service';
import { NotificationType } from '../Models/notificationModel';
import { Customer } from '../Models/customerModels';
import { EmployeeService } from '../Services/employees.service';
import { SearchAdapter } from '../Models/SearchAdapter';
import { Employee } from '../Models/employeeModel';
import { UtilityService } from '../Services/utilities';
import { QuoteApiService } from '../Services/quotesApi.service';
import { DialogService } from '../Services/dialog.service';
import { OrderItemsComponent } from './orderItems.component';
import { formArrayNameProvider } from '@angular/forms/src/directives/reactive_directives/form_group_name';
import * as moment from 'moment';

@Component({
    selector: 'order-details',
    templateUrl: './orderDetails.component.html',
    styleUrls: ['./orderDetails.component.scss']
})
export class OrderDetailsComponent implements OnDestroy, OnInit {
    form: FormGroup;
    orderIsCreated: boolean = true;
    orderId: number = null;
    organizationList$: Observable<Customer[]>;
    employees$: Observable<Employee[]>;
    shippingAccount: string;

    customerDisplay: string; 
    customerEmail: string = '';
    customerMobile: string = '';
    orderDescription: string;
    
    tabActive: boolean = false;
    showGeneral: boolean = true;
    showBadges: boolean = false;
    showShipping: boolean = false;
    showServices: boolean = false;
    showSupport: boolean = false;
    orderSupportTechs: {}[];

    customerContactSelectSettings: any;
    initialCustomerContact:  any;

    quoteSelectSettings: any;
    initialQuote: any;

    onsiteContactSettings: any;
    initialOnsiteContact: any;
    
    orderHWList$: Observable<HardwareRentalItem[]>;
      // this.addedToOrderOrService$ = this.hardwareAPI.addedToOrder();
    kitStatus = false;
    badgeOrderStatus: string = '';
    orderStatus: string = '';
    deleteText : string = 'Delete';
    routerSubscription: any;
    formIsDirty: boolean = false;

    @ViewChild(OrderItemsComponent) hwComponent; 

    get shipDate() {
        return this.form.get('shipDate').value;
    }


    constructor(
        private orderApiService: OrderApiService,
        private customerApiService: CustomerApiService,
        private contactApiService: ContactApiService,
        private quotesApiService: QuoteApiService,
        private search: SearchService,
        private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder,
        private notifier: NotificationService,
        private employees: EmployeeService,
        private hardwareService: HardwareService,
        private utils : UtilityService,
        private dialogService: DialogService
    ) {
        this.customerContactSelectSettings = {
            ajax: {
                delay: 500,
                transport: (params, success,failure) => new SearchAdapter(
                    this.search.searchCustContactAPI(params.data.term),
                    function(contact){ return {id: contact.ContactId, text: contact.FirstName+' '+ contact.LastName}},
                    success, failure),
            }
        };

        this.quoteSelectSettings = {
            ajax: { 
                delay: 500,
                transport: (params,success,failure) => new SearchAdapter(
                    this.quotesApiService.searchQuotesAPI(params.data.term),
                    function(quote){ return {id: quote.quoteNumber, text: quote.quoteNumber+' - '+quote.companyName}},
                    success, failure),
            }
        };

        this.organizationList$ = this.customerApiService.customerList$;
        this.organizationList$.subscribe((org)=> {
            org.sort((a,b) => {
                if(a.name > b.name) {  return  1;};
                if(a.name < b.name) {  return -1;};
                return 0;
            });
        });
        
        this.createForm();
        
        this.form.get('organizationId').valueChanges.pipe(
            combineLatest(this.organizationList$, (newValue,orgList) => {
                if(!orgList) return;
                if(newValue) {
                    const orgObj = orgList.find(org => org.organizationId == newValue);
                    this.customerDisplay = orgObj ? orgObj.shortName : '';
                    this.shippingAccount = orgObj ? orgObj.shippingAccount : '';
                } else {
                    this.customerDisplay = '';
                    this.shippingAccount = '';
                }
            }))
            .subscribe();
        
            this.form.get('customerContactId').valueChanges
            .subscribe(contactId => {
                this.customerEmail = '';
                this.customerMobile = '';
                if(contactId.length < 1){ return;  }
                this.contactApiService.getContact(contactId)
                .subscribe(cust => {
                    this.customerEmail = cust.email;
                    this.customerMobile = cust.mobilePhone;
                });
            });
        
        this.form.get('description').valueChanges
            .subscribe(newValue => this.orderDescription = newValue);
    
        this.form.get('badgeOrderStatus').valueChanges
            .subscribe(kitStatus => {
                this.badgeOrderStatus = kitStatus;
                (kitStatus === '' || kitStatus === "4") && this.orderStatus === "3" ? this.kitStatus = true : this.kitStatus = false; 
            });

        this.form.get('status').valueChanges
            .subscribe(status => {
                this.orderStatus = status;
                (this.badgeOrderStatus === '' || this.badgeOrderStatus === "4") && this.orderStatus === "3" ? this.kitStatus = true : this.kitStatus = false;  
            });
        // LEAVE THIS IN FOR REFERENCE PLS
        // this.routerSubscription = router.events.forEach((event)=> {
        //     if(event instanceof NavigationEnd && !event.url.includes('order-details') && this.formIsDirty){
        //         //console.log('ARE YOUU READY !!!!! closing details');
        //         if(!window.confirm('You are about to lose unsaved changes')){
        //            // this.sa
        //         }

        //     }
        // });

       // send to router guard 
        // this.form.valueChanges.subscribe(val => {
        //     if(!this.form.pristine){
        //         this.formIsDirtyObservable.next(true);
        //     }
        // });
         
    }

 ngOnInit(): void {
     this.employees$ = this.employees.userEmployees$;
     //this.support$ = this.employees.userEmployees$;

     this.orderHWList$ = this.hardwareService.orderHardwareList$;
     this.route.paramMap.pipe(
         map((params) => +(params.get('orderNumber')) ))
         .subscribe((orderNumber) => {
             this.customerApiService.fetchCustomerList();
             this.orderId = orderNumber;
             if(orderNumber == 0) {
                this.updateForm(new Order)
             }
             else {
                this.orderApiService.getOrder(orderNumber)
                    .subscribe(result => {
                        if( result == null ) return;
                        this.updateForm(result);
                        this.hardwareService.getHardwareForOrder(this.orderId);
                    });
             }
         });
 }


 canDeactivate(): Observable<boolean> | boolean {
    this.formIsDirty = true;
    if (!this.form.pristine || this.hwComponent && this.hwComponent.form && !this.hwComponent.form.pristine) {
        return this.dialogService.confirm('YOU WILL LOSE YOUR CHANGES IF YOU CONTINUE');
    }
    return true;
  }	

 @HostListener('window:beforeunload', ['$event'])
 public beforeunloadHandler($event) {
     if(!this.form.pristine){
        $event.returnValue = "Discard changes for this Order?";
     }
}

 private createForm() {
     this.form = this.formBuilder.group({
        // required
         orderId: [0],
         description: ['',Validators.required],
         accountOwner: ['',Validators.required],
         organizationId: [0,Validators.required],
         beginDate: ['',Validators.required],
         projectDueDate: ['',Validators.required],
         status: ['',Validators.required],
         supportBadgeRequired: false,
         hasCompliance: false,
       
         customerContactId: null,
         quoteNumber: '',
         accountManager: '',
         projectLead: '',
         orderDate: '',
         endDate: '',
         shipDate: '',
         arrivalDate: '',
         equipmentReturned: false,
         postShowDeliveryDate: '',
         location: '',
         badgeType: null,
         badgingCompany: '',
         badgeOrderStatus: null,
         badgingService: null,
         badgeKitCost: '',
         badgeKitOrderDate: '',
         badgeKitReceived: false,
         badgeFormatAddedBy: '',
         badgeNotes: '',
         jobNumber: '',
         website: '',
         boothNumber: '',
         venue: '',
         notes: '',
         hours: '',
         shipMethod: null,
         shipResponsibility: '',
         shippingAddress: '',
         trackingNumbers: '',
         returnTrackingNumbers: '',
         shippingNotes: '',
         support: null,
         supportTechs: [],
         supportDateIn: '',
         supportDateOut: '',
         supportHotelClientProvided: '',
         supportHotelBooked: '',
         supportFlightBooked: '',
         supportTravelDateIn: '',
         supportTravelDateOut: '',
         supportHotelAddress: '',
         onSiteContactId: '',
         supportNotes: '',
         complianceInfo: '',
         warehouseStartDate: '',
         warehouseTarget: '',
         warehouseDeadline: '',
     });
 }
 private updateForm(order: Order) {
     const util = this.utils;
     let form = this.form;
     this.orderIsCreated = order.orderId > 0;
    this.initialCustomerContact = order.customerContactId ? [{ id: order.customerContactId, text: order.customerContactName }] : null;
    this.initialQuote = order.quoteNumber ? [{id: order.quoteNumber, text: order.quoteNumber}] : null;
    if(this.utils.noData(this.orderSupportTechs)){ 
        this.orderSupportTechs = new Array<{}>();
    }

    if(!this.utils.noData(order.supportTechs)){
        this.orderSupportTechs = new Array<{}>();
    for(var i = 0; i < order.supportTechs.length; ++i){

        console.log(order.supportTechs[i]);
        this.orderSupportTechs.push(order.supportTechs[i]);
    };
    }

    //this.orderSupportTechs = order.supportTechs;

      form.setValue({
         // required
         orderId                             : order.orderId,
         description                         : order.description,
         accountOwner                        : util.valueOrEmptyStr(order.accountOwner),
         organizationId                      : order.organizationId,
         beginDate                           : order.beginDate,
         projectDueDate                      : order.projectDueDate,
         status                              : order.status,
         supportBadgeRequired                : order.supportBadgeRequired,
         hasCompliance                       : order.hasCompliance,
       
         customerContactId                   : util.valueOrEmptyStr(order.customerContactId),
         quoteNumber                         : order.quoteNumber, 
         accountManager                      : util.valueOrEmptyStr(order.accountManager),
         projectLead                         : order.projectLead ? order.projectLead : '',
         orderDate                           : order.orderDate,
         endDate                             : order.endDate,
         shipDate                            : order.shipDate,
         arrivalDate                         : order.arrivalDate,
         equipmentReturned                   : order.equipmentReturned === null ? false : order.equipmentReturned,
         postShowDeliveryDate                : order.postShowDeliveryDate,
         location                            : order.location,
         badgeType                           : util.valueOrEmptyStr(order.badgeType),
         badgingCompany                      : util.valueOrEmptyStr(order.badgingCompany),
         badgeOrderStatus                    : util.valueOrEmptyStr(order.badgeOrderStatus),
         badgingService                      : util.valueOrEmptyStr(order.badgingService),
         badgeKitCost                        : order.badgeKitCost,
         badgeKitOrderDate                   : order.badgeKitOrderDate,
         badgeKitReceived                    : order.badgeKitReceived,
         badgeFormatAddedBy                  : util.valueOrEmptyStr(order.badgeFormatAddedBy),
         badgeNotes                          : order.badgeNotes,
         jobNumber                           : order.jobNumber,
         website                             : order.website,
         boothNumber                         : order.boothNumber,
         venue                               : order.venue,
         notes                               : order.notes,
         hours                               : order.hours,
         shipMethod                          : util.valueOrEmptyStr(order.shipMethod),
         shipResponsibility                  : util.valueOrEmptyStr(order.shipResponsibility),
         shippingAddress                     : order.shippingAddress,
         trackingNumbers                     : order.trackingNumbers,
         returnTrackingNumbers               : order.returnTrackingNumbers,
         shippingNotes                       : order.shippingNotes,
         support                             : util.valueOrEmptyStr(order.support),
         supportTechs                        : order.supportTechs,
         supportDateIn                       : order.supportDateIn,
         supportDateOut                      : order.supportDateOut,
         supportHotelClientProvided          : order.supportHotelClientProvided === null ? false : order.supportHotelClientProvided, 
         supportFlightBooked                 : order.supportFlightBooked === null ? false : order.supportFlightBooked, 
         supportHotelBooked                  : order.supportHotelBooked === null ? false : order.supportHotelBooked, 
         supportTravelDateIn                 : order.supportTravelDateIn,
         supportTravelDateOut                : order.supportTravelDateOut,
         supportHotelAddress                 : order.supportHotelAddress,
         onSiteContactId                     : order.onSiteContactId,
         supportNotes                        : order.supportNotes,
         complianceInfo                      : order.complianceInfo,
         warehouseStartDate                  : order.warehouseStartDate,
         warehouseDeadline                   : order.warehouseDeadline,
         warehouseTarget                 : order.warehouseTarget
     });
     form.markAsPristine();
    //  this.orderApiService.getSomeOrders('80');
 }
 
 addSupport(thing: any){
   var  supportTech = {};
   var humanPerson = this.form.get('supportTechs').value;
    if(this.utils.noData(this.orderSupportTechs)){ 
        this.orderSupportTechs = new Array<{}>();
    }
    var dupers = this.employees$.subscribe(
        data => { 
            supportTech = data.find(function(e){
                                    return e.userName == humanPerson; 
                                  });
        },
        error => console.log('send this error to the error service', error),
        () => console.log('completed deleted'));
        
        if(!this.utils.noData(supportTech)){
            this.orderSupportTechs.push(supportTech);
        } 
 }

 removeTechSupport(thing: any){
     var supportPerson = this.orderSupportTechs.splice(this.orderSupportTechs.indexOf(thing), 1);
     this.form.markAsDirty();
 }

 onSubmit() {
    if(this.deleteText.includes('sure')){
        this.deleteText = 'Delete';
    }

    if( !this.form.valid ) {
         return;
     }
     let order = this.prepareSaveOrder();
     if(order.orderId > 0){
         this.orderApiService.updateOrder(order).pipe(
             tap(()=> this.notifier.sendNotification('Order successfully updated. ', NotificationType.info)))
             .subscribe((updatedOrder: Order):void => {
                 if( updatedOrder == null ) {
                     return;
                 }
                 this.updateForm(updatedOrder);
             });
     } else {
         this.orderApiService.createOrder(order).pipe(
             tap(()=> this.notifier.sendNotification('Order successfully created. ', NotificationType.info)))
             .subscribe((newOrder: Order):void => {
                 if( newOrder == null ) {
                     return;
                 }
                 this.form.markAsPristine();
                //  this.orderApiService.getSomeOrders('80');
                 this.router.navigate(['../../order-details', newOrder.orderId], {relativeTo:this.route});
             });
     }
 }

onHideOrder(){

    if(this.deleteText.includes('Delete')){
        this.deleteText = 'Are you sure?';
        return;
    } else {
        this.deleteText = 'Delete';
    }

     this.orderApiService.hideOrder(this.orderId)
     .subscribe((didHide) => {
        if(didHide) {
            // this.orderApiService.getSomeOrders('80');
            this.notifier.sendNotification("Order deleted.", NotificationType.info);
            this.router.navigate(['../..'], {relativeTo:this.route});
        }
     });
 }

 private prepareSaveOrder(): Order {
     const util = this.utils;
     const formModel = this.form.value;
     const saveOrder: Order = new Order();

     /* Required */
     saveOrder.orderId					        = formModel.orderId,
     saveOrder.description						= util.trimmedStrOrNull(formModel.description),
     saveOrder.accountOwner						= formModel.accountOwner,
     saveOrder.organizationId					= formModel.organizationId,
     saveOrder.beginDate						= formModel.beginDate,
     saveOrder.projectDueDate					= formModel.projectDueDate,
     saveOrder.status						    = formModel.status,
     saveOrder.supportBadgeRequired				= formModel.supportBadgeRequired === null ? false : formModel.supportBadgeRequired,
     saveOrder.hasCompliance					= formModel.hasCompliance === null ? false : formModel.hasCompliance,
     /* Required */
     
     saveOrder.quoteNumber						= util.valueOrNull(formModel.quoteNumber),
     saveOrder.accountManager					= util.valueOrNull(formModel.accountManager),
     saveOrder.projectLead						= util.valueOrNull(formModel.projectLead),
     saveOrder.orderDate						= util.valueOrNull(formModel.orderDate),
     saveOrder.endDate						    = util.valueOrNull(formModel.endDate),
     saveOrder.shipDate						    = util.valueOrNull(formModel.shipDate),
     saveOrder.arrivalDate						= util.valueOrNull(formModel.arrivalDate),
     saveOrder.equipmentReturned				= formModel.equipmentReturned === null ? false : formModel.equipmentReturned,
     saveOrder.postShowDeliveryDate				= util.valueOrNull(formModel.postShowDeliveryDate),
     saveOrder.location			    			= util.trimmedStrOrNull(formModel.location),
     saveOrder.badgeType						= util.valueOrNull(formModel.badgeType),
     saveOrder.badgingCompany    				= util.valueOrNull(formModel.badgingCompany),
     saveOrder.badgeOrderStatus					= util.valueOrNull(formModel.badgeOrderStatus),
     saveOrder.badgingService					= util.valueOrNull(formModel.badgingService),
     saveOrder.badgeKitCost						= util.valueOrNull(formModel.badgeKitCost),
     saveOrder.badgeKitOrderDate				= util.valueOrNull(formModel.badgeKitOrderDate),
     saveOrder.badgeKitReceived				    = formModel.badgeKitReceived === null ? false : formModel.badgeKitReceived,
     saveOrder.badgeFormatAddedBy               = util.valueOrNull(formModel.badgeFormatAddedBy),
     saveOrder.badgeNotes						= util.trimmedStrOrNull(formModel.badgeNotes),
     saveOrder.customerContactId                = util.valueOrNull(formModel.customerContactId),
     saveOrder.jobNumber				    	= util.trimmedStrOrNull(formModel.jobNumber),
     saveOrder.website					    	= util.trimmedStrOrNull(formModel.website),
     saveOrder.boothNumber						= util.trimmedStrOrNull(formModel.boothNumber),
     saveOrder.venue						    = util.trimmedStrOrNull(formModel.venue),
     saveOrder.notes						    = util.trimmedStrOrNull(formModel.notes),
     saveOrder.hours						    = util.trimmedStrOrNull(formModel.hours),
     saveOrder.shipMethod					    = util.valueOrNull(formModel.shipMethod),
     saveOrder.shipResponsibility				= util.valueOrNull(formModel.shipResponsibility),
     saveOrder.shippingAddress					= util.trimmedStrOrNull(formModel.shippingAddress),
     saveOrder.trackingNumbers					= util.trimmedStrOrNull(formModel.trackingNumbers),
     saveOrder.returnTrackingNumbers            = util.trimmedStrOrNull(formModel.returnTrackingNumbers),
     saveOrder.shippingNotes					= util.trimmedStrOrNull(formModel.shippingNotes),
     saveOrder.support						    = util.valueOrNull(formModel.support),
     saveOrder.supportTechs						= util.valueOrNull(this.orderSupportTechs),
     saveOrder.supportDateIn					= util.valueOrNull(formModel.supportDateIn),
     saveOrder.supportDateOut					= util.valueOrNull(formModel.supportDateOut),
     saveOrder.supportHotelClientProvided       = formModel.supportHotelClientProvided === null ? false : formModel.supportHotelClientProvided,
     saveOrder.supportHotelBooked       		= formModel.supportHotelBooked === null ? false : formModel.supportHotelBooked,
     saveOrder.supportFlightBooked           	= formModel.supportHotelFlightBooked === null ? false : formModel.supportFlightBooked,
     saveOrder.supportTravelDateIn				= util.valueOrNull(formModel.supportTravelDateIn),
     saveOrder.supportTravelDateOut				= util.valueOrNull(formModel.supportTravelDateOut),
     saveOrder.supportHotelAddress				= util.trimmedStrOrNull(formModel.supportHotelAddress),
     saveOrder.onSiteContactId					= util.valueOrNull(formModel.onSiteContactId),
     saveOrder.supportNotes						= util.trimmedStrOrNull(formModel.supportNotes),
     saveOrder.complianceInfo					= formModel.complianceInfo === null ? false : formModel.complianceInfo,
     saveOrder.warehouseDeadline                = util.valueOrNull(formModel.warehouseDeadline), 
     saveOrder.warehouseStartDate               = util.valueOrNull(formModel.warehouseStartDate), 
     saveOrder.warehouseTarget                  = util.valueOrNull(formModel.warehouseTarget) 
     
     /* Required */
     return saveOrder;
 }
tab: number = 0;

setTabInterceptor(num:number){
    if (!this.form.pristine) {
         this.dialogService.confirm('YOU WILL LOSE YOUR CHANGES IF YOU CONTINUE').subscribe(dlgValue => {
             if(dlgValue) {
                 this.setTab(num);
             }
         });
    }
    else{
        this.setTab(num);
    }
}
setTab (num:number) {
    // if (!this.form.pristine) {
    //     var dlg = this.dialogService.confirm('YOU WILL LOSE YOUR CHANGES IF YOU CONTINUE');
    //     if(!dlg)
    //         return;// this.dialogService.confirm('YOU WILL LOSE YOUR CHANGES IF YOU CONTINUE');
    // }
    this.tab = num;
    this.showGeneral = false;
    this.showBadges = false;
    this.showServices = false; 
    this.showShipping = false;
    this.showSupport = false;
    // this.showShipping = false;
    switch(num){
        case 0:
            this.showGeneral = true;
            break;
        case 1: 
            this.showBadges = true;
            break;
        case 2:
            this.showShipping = true;
            break;
        case 3:
            this.showServices = true;
            break;
        case 4: 
            this.showSupport = true;
            break;
    }
}

isSelected(num: number) {
    return this.tab === num;
}

myOnHover(){
    var cust = document.getElementById('toolytip');
    cust.style.display = 'block';
}
myOnLeave(){
    var cust = document.getElementById('toolytip');
    cust.style.display = 'none';
}

calculateProjectDue(){
    var eventStartDate = this.form.get('beginDate');
    var projectDueDate = this.form.get('projectDueDate');
    if(eventStartDate.value !== null && projectDueDate.value === null){ // we only want to enforce the blackout rules if it doesnt have a date already
        var t = moment.utc(eventStartDate.value, moment.ISO_8601).subtract(21, 'd').format('YYYY-MM-DD');
        return t;
    }
    return null;
}
 ngOnDestroy() {
    //  if(this.routerSubscription){
    //     this.routerSubscription.unsubscribe();
    //  }
 }


}