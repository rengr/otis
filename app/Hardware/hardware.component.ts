import { HardwareListItem, HardwareRentalItem } from "../Models/hardwareModel";
import { Component, OnDestroy, OnInit, Input } from "@angular/core";
import { Observable ,  Subject ,  BehaviorSubject } from "rxjs";
import { FormGroup, FormControl, Validators, FormBuilder, RequiredValidator} from "@angular/forms";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import * as moment from "moment";
import { HardwareService } from "../Services/hardware.service";
import { NotificationService } from "../Services/notification.service";
import { NotificationType } from "../Models/notificationModel";
import { UtilityService } from "../Services/utilities";
import { LogService } from "../Services/log.service";

@Component({
  templateUrl: "./hardware.component.html",
  styleUrls: ['./hardware.component.scss'],
  selector: "hardware"
})
export class HardwareComponent implements OnDestroy, OnInit {

  private _orderId: number = null;
  @Input()
  set orderId(orderId: number) {
    this._orderId = orderId;
  }

  private _currentOrderItemId: number = null;
  @Input()
  set currentOrderItemId(currentOrderItemId: number) {
    this._currentOrderItemId = currentOrderItemId;
    this.getServiceHardware(this._currentOrderItemId);
  }
  allHardware$: Observable<HardwareListItem[]>;
  orderItemHardware$ = new  BehaviorSubject<HardwareRentalItem[]>([]);
  
  form: FormGroup;
  currentItemId: number = null;
  showDetails: boolean = false;
  showDelete: boolean = false;

  constructor(
    private hardwareApi: HardwareService,
    private notifier: NotificationService,
    private utils: UtilityService,
    private formBuilder: FormBuilder,
    private log: LogService,
  ) {
    this.allHardware$ = hardwareApi.hardwareList$;
    this.form = formBuilder.group({
      rentalProductId: ['', RequiredValidator],
      quantity: [0, RequiredValidator],
      backupDevices: [0],
      notes: [null]
    });
  }

  ngOnInit(): void {
  }

  getServiceHardware(orderItemId: number) {
    if (orderItemId === null) {
      return;
    }
    this.hardwareApi
      .getHardwareForService(orderItemId)
      .subscribe(hardwareItemArray => {
        this.orderItemHardware$.next(hardwareItemArray);
      });
  }
  private doCreate() {
    if(this.form.dirty && !confirm("Are you sure you want to leave and lose your changes?")) { return; }
    this.updateForm(new HardwareRentalItem());
  }
  private doEdit(row: HardwareRentalItem) {
    if(!row) { return; }
    if(this.form.dirty && !confirm("Are you sure you want to leave and lose your changes?")) { return; }
    this.updateForm(row);
  }
  private onDeleteCurrentItem() {
    if (confirm("Are you sure you want to delete this hardware item?")) {
      this.hardwareApi.deleteHardwareRental(this.currentItemId, this._orderId)
        .subscribe(
          didDelete => {
            this.showDetails = false;
            this.showDelete = false;
            this.currentItemId = null;
            this.getServiceHardware(this._currentOrderItemId);
            this.hardwareApi.getHardwareForOrder(this._orderId);
          }),
          (error) => {
            this.log.logMessage('Could not delete hardware. (' + error.error + ')', NotificationType.error);
          }
    }
  }
  private updateForm(newItem: HardwareRentalItem) {
    this.currentItemId = newItem.itemId;
    this.form.setValue({
      rentalProductId: newItem.rentalProductId,
      quantity: newItem.quantity,
      backupDevices: newItem.backupDevices,
      notes: newItem.notes
    });
    this.form.markAsPristine();
    this.showDetails = true;
    this.showDelete = newItem.itemId > 0;
    if(newItem.itemId > 0) {
      this.form.get('rentalProductId').disable();
    } else {
      this.form.get('rentalProductId').enable();
    }
  }
  onSubmit() {
    if (this.currentItemId == null || this.currentItemId < 0 || !this.form.dirty) { return; }
    if (!this._orderId || !this._currentOrderItemId) {
      this.notifier.sendNotification(
        "Cannot save rental. The hardware component does not have a orderId and orderItemId set.",
        NotificationType.error);
      return;
    }  
    if(!this.form.valid) {
      this.notifier.sendNotification(
        "Cannot save rental. Some fields have missing or incorrect values.",
        NotificationType.warning
      );
      return;
    }

    let item = new HardwareRentalItem();
    const util = this.utils;
    const formValue = this.form.value;
   
    item.itemId          = this.currentItemId;
    item.orderId         = this._orderId;
    item.orderItemId     = this._currentOrderItemId;
    item.rentalProductId = this.form.get('rentalProductId').value;
    item.quantity        = formValue.quantity === '' || formValue.quantity === null ? 0 : formValue.quantity;
    item.backupDevices   = formValue.backupDevices;
    item.notes           = formValue.notes;

    if( item.itemId < 1 ) {
      this.hardwareApi.createHardwareRental(item)
        .subscribe(newRentalItem => {
          if(newRentalItem == null ) return;
          this.updateForm(newRentalItem);
          this.showDetails = false;
          this.showDelete = false;
          this.currentItemId = null;
          this.getServiceHardware(this._currentOrderItemId);
          this.hardwareApi.getHardwareForOrder(this._orderId);
        });
    }
    else {
      this.hardwareApi.updateHardwareRental(item)
      .subscribe(
        stuff => {
          this.updateForm(item);
          this.showDetails = false;
          this.showDelete = false;
          this.currentItemId = null;
          this.getServiceHardware(this._currentOrderItemId);
          this.hardwareApi.getHardwareForOrder(this._orderId);
        },
        error => {
          this.log.logMessage('Could not update hardware. (' + error.error + ')', NotificationType.error);
        }
      );
    }
  }

  ngOnDestroy(): void {}
}
