import { SearchService } from '../Services/search.service';
import { CommonModule } from '@angular/common';
import { Component, OnDestroy, OnInit} from '@angular/core';
//import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Observable ,  BehaviorSubject ,  Subject } from 'rxjs';
import { Router } from '@angular/router';

import { ContactApiService } from '../Services/contactsApi.service';
import { Contact } from '../Models/contactModel';
import { NotificationService } from '../Services/notification.service';


@Component({
    templateUrl: './contacts.component.html',
    styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent implements OnDestroy, OnInit {

    headers$: Observable<string[]>;
    columns$: Observable<string[]>;
    contacts$: Contact[]; 
    showDetailsBoolean: boolean = false;
    searchTerm$ = new Subject<string>();

    constructor(
        private ContactService: ContactApiService,
        private router: Router,
        private search: SearchService,
        private notification: NotificationService
    ) {
        this.headers$ = new BehaviorSubject([
            'First Name',
            'Last Name',
            'Company',
            'Email',
            'Job Title',
            'Work Phone',
            'Mobile Phone',
            'Partner',
            'Notes'
        ]);
        this.columns$ = new BehaviorSubject([
            'firstName',
            'lastName',
            'company',
            'email',
            'jobTitle',
            'workPhone',
            'mobilePhone',
            'partnerName',
            'notes'
        ]);
    }

    ngOnInit(): void {
        this.ContactService.contactList$.subscribe(
                data => {
                    // data.map(data => this.m);a
                    data.sort((a,b) => {
                        if(a.lastName.toLowerCase() > b.lastName.toLowerCase()) {  return  1;};
                        if(a.lastName.toLowerCase() < b.lastName.toLowerCase()) {  return -1;};
                        return 0;
                    });            
                    this.contacts$ = data;
                    // this.cu$.sort(this.comparer);
                },
                error => console.log('send this error to the error service', error),
                () => console.log('completed deleted'));
        
        this.ContactService.fetchContactList();

        this.ContactService.searchContacts(this.searchTerm$);

        this.ContactService.announcingChangedContact$.subscribe(contact => {
                var searchTerm = <HTMLInputElement>document.getElementById('searchBox');
                this.searchTerm$.next(searchTerm.value);
        });
    }

    ngOnDestroy() {

    }

    onButtonClick(Contact: Contact) {
        this.router.navigate(['/Contact-details', Contact.contactId]);
    }
    
    onActivate(event: any) {
        this.showDetailsBoolean = true;
    }
    onDeactivate(event: any) {
        this.showDetailsBoolean = false;
    }

}