import { Component, OnDestroy, OnInit, HostListener } from '@angular/core';
import { NotificationService } from '../Services/notification.service';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap} from '@angular/router';
import { Observable ,  BehaviorSubject } from 'rxjs';


import { ContactApiService } from '../Services/contactsApi.service';
import { Contact } from '../Models/contactModel';

import { CustomerApiService } from '../Services/customerApi.service';
import { Customer } from '../Models/customerModels'
import { Employee } from '../Models/employeeModel';
import { EmployeeService } from '../Services/employees.service';
import { SearchAdapter } from '../Models/SearchAdapter';
import { UtilityService } from '../Services/utilities';
import { DialogService } from '../Services/dialog.service';

@Component({
    selector: 'Contact-details',
    templateUrl: './contactDetails.component.html',
})
export class ContactDetailsComponent implements OnDestroy, OnInit {
    title: string;
    contactId: number;
    form: FormGroup;
    linkedOrgSettings: any;
    initialLinkedOrgs: any;

    constructor(
        private contactService: ContactApiService,
        private orgService: CustomerApiService,
        private route: ActivatedRoute,
        private router: Router,
        private formBuilder: FormBuilder,
        private utils: UtilityService,
        private dialogService: DialogService
 
    ) {
        this.linkedOrgSettings = {
            multiple: true,
            ajax: {
                delay: 500,
                transport: (params,success,failure) => new SearchAdapter(
                    this.orgService.searchCustomersAPI(params.data.term),
                    function(org){
                        return {id: org.organizationId, text: org.shortName};
                    },
                    success, failure, false),
            }
        };
        this.createForm();
    }



    ngOnInit(): void {
        this.route.paramMap.subscribe((params) => {
            this.contactId = +(params.get('contactId'));
            if( this.contactId > 0 ) { // new Contact form.
                this.title = "Contact";
                this.contactService.getContact(this.contactId)
                    .subscribe(result => {
                        this.updateForm(result);
                    });
            }
            else {
                this.title = "New Contact";
                this.form.reset();
            }
            // this.orgService.customerList$.subscribe(orgList => {
            //     this.organizationList$.next(orgList);
            // })
            // this.orgService.fetchCustomerList()
        });
    }

    private createForm() {
        this.form = this.formBuilder.group({
            firstName: ['',Validators.required],
            lastName: ['',Validators.required],
            company: ['',Validators.required],
            email: '',
            jobTitle: '',
            workPhone: '',
            mobilePhone: '',
            notes: '',
            linkedOrganizations: [],
            assocPartner: null
        });
    }
    private updateForm(contact: Contact) {
        let form = this.form;
        let linkedOrgValue = [];
        let initLinkOrgs = [];
        if(contact.linkedOrganizations) {
            contact.linkedOrganizations.forEach(linkedOrg => {
                initLinkOrgs.push({id: linkedOrg.id, text: linkedOrg.name});
                linkedOrgValue.push(linkedOrg.id);
            });
        }
        this.initialLinkedOrgs = initLinkOrgs;

        form.setValue({
            firstName: contact.firstName,
            lastName: contact.lastName,
            company: contact.company,
            jobTitle: contact.jobTitle,
            email: contact.email,
            mobilePhone: contact.mobilePhone,
            workPhone: contact.workPhone,
            notes: contact.notes,
            linkedOrganizations: linkedOrgValue,
            assocPartner: contact.partnerId,
        });
     form.markAsPristine();
    }

    onSubmit() {
        if(this.form.invalid) { return; }

        let contact = this.prepareSaveContact();
        if(contact === null ) { return ; }

        let request:Observable<Contact> = null;
        if( contact.contactId == 0 ) {
            request = this.contactService.createContact(contact);
        }
        else { request = this.contactService.updateContact(contact); }
        
        request
        .subscribe((updatedContact: Contact):void => {
            if(updatedContact.contactId != this.contactId) {
                this.router.navigate(['../../contact-details',updatedContact.contactId], {relativeTo:this.route});
            }
            this.updateForm(updatedContact);
        });
    }


    // onDelete(){
    //     this.ContactService.deleteContact(this.form.value.contactId)
    //         .subscribe(
    //             data => { 
    //                 this.notification.sendNotification('item has been deleted', null);
    //                 this.router.navigate(['/customers']);
    //             },
    //             error => console.log('send this error to the error service', error),
    //             () => console.log('completed deleted')
    //         );
    // }

    private prepareSaveContact(): Contact {
        if( !this.form.valid ) {
            alert("Form isn't valid!");
            return;
        }
        const util = this.utils;
        const formModel = this.form.value;
        const saveContact: Contact = new Contact();
       /* Required */
        saveContact.contactId = this.contactId;
        saveContact.firstName = formModel.firstName;
        saveContact.lastName = formModel.lastName;
        saveContact.company = formModel.company;
        /* Required */
        saveContact.partnerId = util.valueOrNull(formModel.assocPartner),
        saveContact.email = util.trimmedStrOrNull(formModel.email);
        saveContact.jobTitle = util.trimmedStrOrNull(formModel.jobTitle);
        saveContact.workPhone = util.trimmedStrOrNull(formModel.workPhone);
        saveContact.mobilePhone = util.trimmedStrOrNull(formModel.mobilePhone);
        saveContact.notes = util.trimmedStrOrNull(formModel.notes);

        if(formModel.linkedOrganizations) {
            let netArr = [];
            (<string[]>formModel.linkedOrganizations)
                .forEach((val) => netArr.push(Number(val)));
            saveContact.linkedOrganizations = netArr;
        }

        return saveContact;
    }

    
 canDeactivate(): Observable<boolean> | boolean {
    if (!this.form.pristine) {
        return this.dialogService.confirm('YOU WILL LOSE YOUR CHANGES IF YOU CONTINUE');
    }
    return true;
  }	

 @HostListener('window:beforeunload', ['$event'])
 public beforeunloadHandler($event) {
     if(!this.form.pristine){
        $event.returnValue = "Discard changes for this Order?";
     }
}

    ngOnDestroy() {

    }
}