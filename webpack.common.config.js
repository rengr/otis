// COMMON WEBPACK COMMANDS
// *****
// webpack-dev-server == start
// webpack -p ==> minify
// webpack -d ==> debugging, sourcemaps, full files
// webpack --config webpack.production.config.js -p
// webpack --progress
// *****

var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
var helpers = require('./helpers');

const extractSass = new ExtractTextPlugin({
  filename: "[name].[hash].css",
  disable: process.env.NODE_ENV === "development"
});

module.exports = {
  entry: {
    app: './app/boot.ts',
  },
  resolve: {
    extensions: ['.ts', '.js']
  },
  module: {
    rules: [
      {
        test: /\.ts$/, 
        use: ['ts-loader',
            'angular2-template-loader'],
        exclude: [/\.(spec|e2e)\.ts$/],
      },
      { /* Load HTML templates */
        test: /\.html$/,
        use: 'html-loader'
      },
      {
        test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
        use: 'file-loader?name=assets/[name].[hash].[ext]'
      },
      {
        test: /\.(css|sass|scss)$/,
        // exclude: helpers.root('app', 'css'),
        // use: extractSass.extract({
          use: [{
            loader: 'raw-loader'
          }, {
            loader: 'sass-loader'
          }],
        // })
      }
    ]
  },
    plugins: [
    new CleanWebpackPlugin(['dist']),
    // Workaround for angular/angular#11580
    new webpack.ContextReplacementPlugin(
        // The (\\|\/) piece accounts for path separators in *nix and Windows
        /angular(\\|\/)core(\\|\/)@angular/,
        helpers.root('./app'), // location of your src
        {} // a map of your routes
    ),
    new HtmlWebpackPlugin({
      template: './index.html'
    }),
    extractSass,
		new CopyWebpackPlugin([
			'node_modules/bootstrap/dist/css/bootstrap.css',
			'node_modules/bootstrap/dist/css/bootstrap.css.map',
			'app/css/sidebar-styles.css',
			'app/css/slide-new.css',
      {from:'node_modules/core-js/client/shim.min.js', to:'core-js.min.js'},
			'node_modules/core-js/client/shim.min.js.map',
			'node_modules/zone.js/dist/zone.js',
			'node_modules/reflect-metadata/Reflect.js',
			'node_modules/reflect-metadata/Reflect.js.map',
      'node_modules/moment/moment.js',
      'web.config',
      //'app/img'
    ]),
    
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /en/),
    new webpack.DefinePlugin({
      'BUILD_DATE': JSON.stringify((new Date()).toLocaleString()),
    }),
    function(){
      const RED = '\033[0;31m';
      const GREEN ='\033[0;32m';
      const NC ='\033[0m';
      this.plugin('watch-run', function(watching, callback){
          const time = new Date();
          console.log(
            `____________________________\n`,
            `last compiled: ${GREEN}${time.getHours()}:${RED}${("0" + time.getMinutes()).slice(-2)}:${("0" + time.getSeconds()).slice(-2)} ${GREEN}***${NC}`,
            `\n____________________________\n`,
          );
          callback();
      })
  }
  ],
  optimization: {
    splitChunks: {
      cacheGroups: {
        vendor: {
          chunks: 'initial',
          name: 'vendor',
          test: /[\\/]node_modules[\\/]/,
          enforce: true
        },
      }
    },
    runtimeChunk: true
  },
  output: {
    publicPath: '/',
    path: helpers.root('dist'),
    filename: '[name].bundle_[hash].js', // filename: '[name].[hash].js'
    chunkFilename: '[name].bundle_[hash].js',
    hashFunction: 'sha256'
  },
 };


// FROM PLURALSIGHT

// module.exports = {
// 	entry: ["./utils", "./app/boot.js"],
// 	output: {
// 		filename: "bundle.js"
// 	},

// 	module: {
// 		loaders: [
// 			{
// 				test: /\.es6$/,
// 				exclude: /node_modules/,
// 				loader: "babel-loader"
// 			}
// 		]
// 	},

// 	resolve: {
// 		extensions: ['', '.js', '.es6']
// 	}
// }

// FROM MIRKONASATO(sp)

// var webpack = require('webpack');
// var HtmlWebpackPlugin = require('html-webpack-plugin');

// module.exports = {

//   entry: {
//     'app': './src/main.ts',
//     'polyfills': [
//       'core-js/es6',
//       'core-js/es7/reflect',
//       'zone.js/dist/zone'
//     ]
//   },
//   output: {
//     path: './dist',
//     filename: '[name].[hash].js'
//   },
//   module: {
//     loaders: [
//       {test: /\.component.ts$/, loader: 'ts!angular2-template'},
//       {test: /\.ts$/, exclude: /\.component.ts$/, loader: 'ts'},
//       {test: /\.html$/, loader: 'raw'},
//       {test: /\.css$/, loader: 'raw'}
//     ]
//   },
//   resolve: {
//     extensions: ['', '.js', '.ts', '.html', '.css']
//   },
//   plugins: [
//     new webpack.optimize.CommonsChunkPlugin({
//       name: 'polyfills'
//     }),
//     new HtmlWebpackPlugin({
//       template: './src/index.html'
//     }),
//     new webpack.DefinePlugin({
//       app: {
//         environment: JSON.stringify(process.env.APP_ENVIRONMENT || 'development')
//       }
//     })
//   ]
  
// };
