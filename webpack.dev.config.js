// COMMON WEBPACK COMMANDS
// *****
// webpack-dev-server == start
// webpack -p ==> minify
// webpack -d ==> debugging, sourcemaps, full files
// webpack --config webpack.production.config.js -p
// webpack --progress
// *****

var webpack = require('webpack');
var webpackMerge = require('webpack-merge');
var Visualizer = require('webpack-visualizer-plugin');
var helpers = require('./helpers');
var commonConfig = require('./webpack.common.config');

module.exports = webpackMerge(commonConfig, {
  devtool: 'inline-source-map',
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
    new webpack.DefinePlugin({
    //  'SERVICE_BASEURL': JSON.stringify("http://localhost:57556/"),
    //  'SERVICE_BASEURL': JSON.stringify("http://taiko2.reality.local:4000/"),
      'SERVICE_BASEURL': JSON.stringify("https://wms.rengr.co:/")
    }),
    new Visualizer(),
        
  ],
  devServer: {
    hot: true,
    host: '127.0.0.1',
    port: 8080,
    historyApiFallback: true,
    watchContentBase: true
  },
 });
