
const webpack = require('webpack');
const webpackMerge = require('webpack-merge');
const WebpackStrip = require('strip-loader');

var UglifyJSPlugin = require('uglifyjs-webpack-plugin');
var commonConfig = require('./webpack.common.config');

module.exports = webpackMerge(commonConfig, {
	module: {
		rules: [
			{
				test: [/\.js$/, /\.es6$/],
				exclude: /node_modules/,
				loader: WebpackStrip.loader('console.log')
			}
		],
	},
  plugins: [
		new webpack.HashedModuleIdsPlugin(),
    new webpack.DefinePlugin({
			'process.env': { 'NODE_ENV' : JSON.stringify('production')},
      'SERVICE_BASEURL': JSON.stringify("https://wms.rengr.co:/")
		}),
		// new UglifyJSPlugin(),
	],
	
});
